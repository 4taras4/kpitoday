//
//  ABLoadServer.m
//  APIDZ-46
//
//  Created by tarik on 12.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABLoadServer.h"

@implementation ABLoadServer

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {
    
        self.uploadUrl = [responseObject objectForKey:@"upload_url"];
        
        
    }
    return self;
}


@end
