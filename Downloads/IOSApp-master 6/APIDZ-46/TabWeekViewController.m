//
//  TabWeekViewController.m
//  Today
//
//  Created by tarik on 15.03.16.
//  Copyright © 2016 tarik. All rights reserved.
//

#import "TabWeekViewController.h"
#import <XCDYouTubeVideoPlayerViewController.h>
#import "ABYoutubeVC.h"
#import <HCYoutubeParser.h>
#import <SWRevealViewController.h>
#import "ABAuthorizationVC.h"
#import <Masonry.h>
#import <EVTTabPageScrollView.h>
#import "JSONModel.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "CheduleViewCell.h"
#import "showDetailsteacherViewController.h"
#import "CheduleModel.h"
#import "JSONModel.h"
#import "LGAlertView.h"
#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVFoundation.h>




@interface TabWeekViewController ()

@property (strong, nonatomic) NSArray<CheduleModel *>* loans;

@property (nonatomic,strong) EVTTabPageScrollView *pageScroll;
@property (strong, nonatomic) UITableView *tableView;
//@property (strong, nonatomic) NSDictionary *tempDictionary;
@property (strong, nonatomic) NSArray *arrayGroups;
@property (strong, nonatomic) NSArray *arrayFromAFNetworking;
@property (strong, nonatomic) NSArray *arrayFromAFNetworking2;
@property (strong, nonatomic) NSMutableArray *responseData;
@property (strong, nonatomic) LGAlertView *securityAlertView;
@property (strong, nonatomic) NSArray *weekDay;
@property (assign, nonatomic) id<LGAlertViewDelegate> delegate;
@property (strong, nonatomic) NSString *groupName;
@property (strong,  nonatomic) NSString *InstituteName;
@property(nonatomic, retain)NSArray *dataStoredMonday;
@property(nonatomic, retain)NSArray *dataStoredTuesday;
@property(nonatomic, retain)NSArray *dataStoredWednesday;
@property(nonatomic, retain)NSArray *dataStoredThersday;
@property(nonatomic, retain)NSArray *dataStoredFriday;

@end

@implementation TabWeekViewController{
    NSUserDefaults *defaults;
    UIPickerView *groupPicker;
}



- (void)viewDidLoad
{
      [self loadGroups];
    [self checkDefaults];
    [self readDataMonday];
    [self readDataTuesday];
    [self readDataWednesday];
    [self readDataTherday];
    [self readDataFriday];
  
    
//    [self makeRequestsWeek];
    [super viewDidLoad];
    [self drawTabs];
    CGRect frame = self.view.bounds;
    frame.origin = CGPointZero;
  
//    UITableView* tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
//
//    self.tableView.delegate = self;
//    self.tableView.dataSource = self;
//    tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    UIBarButtonItem* editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
//                                                                                target:self
//                                                                                action:@selector(actionEdit:)];
////    [editButton setImage:[UIImage imageNamed:@"setting.png"]];
//    self.navigationItem.rightBarButtonItem = editButton;
    //self.arrayFromAFNetworking = [NSMutableArray array];
    
    
 
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        [self.sidebarButton setTarget:self.revealViewController];
        [self.sidebarButton setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -  SegmentControllUI



#pragma mark - JSON PArcing&Sorting
-(void)checkDefaults{
    defaults = [NSUserDefaults standardUserDefaults];
    NSObject * object = [defaults objectForKey:@"week"];
    if(object == nil){
        NSLog(@"Defaultf nill");
        NSInteger *week = 1;
        NSString *weekStrig = [[NSString alloc] initWithFormat:@"%li",(long)week];
        [defaults setObject:weekStrig forKey:@"week"];
        [defaults synchronize];
    }
}
-(void)drawTabs{

    // Section Mon
    sectionOne = [[UITableView alloc] init];
    [sectionOne setBackgroundColor:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0]];
    [sectionOne setTag:1];
    [sectionOne setDelegate:self];
    [sectionOne setDataSource:self];
    [sectionOne reloadData];
   
    
    // Section Tue
    sectionTwo = [[UITableView alloc] init];
    [sectionTwo setBackgroundColor:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0]];
    [sectionTwo setTag:2];
    [sectionTwo setDelegate:self];
    [sectionTwo setDataSource:self];
    [sectionTwo reloadData];
 
    
    // Section Wed
    sectionThree = [[UITableView alloc] init];
    [sectionThree setBackgroundColor:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0]];
    [sectionThree setTag:3];
    [sectionThree setDelegate:self];
    [sectionThree setDataSource:self];
    [sectionThree reloadData];

    // Section Thu
    
    sectionFour = [[UITableView alloc] init];
    [sectionFour setBackgroundColor:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0]];
    [sectionFour setTag:4];
    [sectionFour setDelegate:self];
    [sectionFour setDataSource:self];
    
    // Section Fri
    sectionFive = [[UITableView alloc] init];
    [sectionFive setBackgroundColor:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0]];
    [sectionFive setTag:5];
    [sectionFive setDelegate:self];
    [sectionFive setDataSource:self];
    
    NSArray *pageItems = @[
                           [[EVTTabPageScrollViewPageItem alloc]initWithTabName:@"ПН" andTabView:sectionOne],
                           [[EVTTabPageScrollViewPageItem alloc]initWithTabName:@"ВТ" andTabView:sectionTwo],
                           [[EVTTabPageScrollViewPageItem alloc]initWithTabName:@"СР" andTabView:sectionThree],
                           [[EVTTabPageScrollViewPageItem alloc]initWithTabName:@"ЧТ" andTabView:sectionFour],
                           [[EVTTabPageScrollViewPageItem alloc]initWithTabName:@"ПТ" andTabView:sectionFive],
                           ];
    _pageScroll = [[EVTTabPageScrollView alloc]initWithPageItems:pageItems];
    
    UIView *rootView = self.view;
    [rootView addSubview:_pageScroll];
    [_pageScroll mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(rootView.mas_top).offset(65);
        make.left.equalTo(rootView.mas_left);
        make.right.equalTo(rootView.mas_right);
        make.bottom.equalTo(rootView.mas_bottom);
    }];


}

-(void)loadGroups{
    
    NSString *urlString = @"http://api.rozklad.org.ua/v2/groups";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request ];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation  setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.arrayGroups = [responseObject objectForKey:@"data"];
//        NSMutableArray *index0 = [self.arrayGroups valueForKey:@"group_full_name"];
        NSLog(@"group loaded: %@",  self.arrayGroups);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Request Failed: %@, %@", error, error.userInfo);
        
    }];
    [operation start];
    
    
}
-(void)makeRequestsWeek{
//        NSString *urlString = @"http://api.rozklad.org.ua/v1/groups/ia-23/lessons";
//        NSURL *url = [NSURL URLWithString:urlString];
//        urlString = [urlString stringByAppendingString:@"lessons"];
//        url = [NSURL URLWithString:urlString];
//    //    self.InstituteName = @"11";
//        defaults = [NSUserDefaults standardUserDefaults];
//        NSString *userName = [defaults objectForKey:@"picker0"];
//        NSInteger height = [defaults integerForKey:@"week"];
//        NSString *countWeek = [NSString stringWithFormat:@"%li",(long)height];
//        self.groupName = userName;
////        self.groupName = @"ia-23";
//        urlString = [NSString stringWithFormat:@"http://api.rozklad.org.ua/v1/groups/%@/", _groupName];
//        url = [NSURL URLWithString:urlString];
//        NSURL *baseURL = [NSURL URLWithString:urlString];
//        NSDictionary *parameters = @{@"week": @"1"};
//     if (sectionOne.tag == 1) {
//        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
//        manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//        [manager GET:@"lessons/" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
//            self.arrayFromAFNetworking = responseObject;
//            
//            [sectionOne reloadData];
//            
//       
//            NSLog(@"lessons : %@", _arrayFromAFNetworking);
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Виберіть групу в налаштуваннях"
//                                                                message:[error localizedDescription]
//                                                               delegate:nil
//                                                      cancelButtonTitle:@"Ok"
//                                                      otherButtonTitles:nil];
//            [alertView show];
//    
//        }];
//     }
//     else if(sectionTwo.tag == 2){
//         AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
//         manager.responseSerializer = [AFJSONResponseSerializer serializer];
//         
//         [manager GET:@"lessons/" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
//             self.dataStoredTuesday = responseObject;
//             
//             [sectionTwo reloadData];
//             
//             
//             NSLog(@"lessons : %@", _arrayFromAFNetworking);
//         } failure:^(NSURLSessionDataTask *task, NSError *error) {
//             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Виберіть групу в налаштуваннях"
//                                                                 message:[error localizedDescription]
//                                                                delegate:nil
//                                                       cancelButtonTitle:@"Ok"
//                                                       otherButtonTitles:nil];
//             [alertView show];
//             
//         }];
//
//     }

}
- (void)makeRequestsWeek2{
    NSString *urlString = @"http://api.rozklad.org.ua/v1/groups/ia-23/lessons";
    NSURL *url = [NSURL URLWithString:urlString];
    urlString = [urlString stringByAppendingString:@"lessons"];
    url = [NSURL URLWithString:urlString];
  
    self.groupName = @"ia-23";
    urlString = [NSString stringWithFormat:@"http://api.rozklad.org.ua/v1/groups/%@/", _groupName];
    url = [NSURL URLWithString:urlString];
    NSURL *baseURL = [NSURL URLWithString:urlString];
    NSDictionary *parameters = @{@"week": @"2"};
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:@"lessons/" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        self.arrayFromAFNetworking = responseObject;
        NSLog(@"%@", _arrayFromAFNetworking);
//        [sectionOne reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Data"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }];
}


#pragma mark - Setting Buttons
- (IBAction)setting:(id)sender {
    [[[LGAlertView alloc] initWithTitle:@"Title"
                                message:@"Message"
                                  style:LGAlertViewStyleActionSheet
                           buttonTitles:@[@"Нагадування"]
                      cancelButtonTitle:@"Cancel"
                 destructiveButtonTitle:@"Обрати групу"
                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                              UIDatePicker *datePicker = [UIDatePicker new];
                              datePicker.datePickerMode = UIDatePickerModeTime;
                              datePicker.minuteInterval = 10;
                              datePicker.frame = CGRectMake(0.f, 0.f, datePicker.frame.size.width, 160.f);
                              
                              [[[LGAlertView alloc] initWithViewAndTitle:@"Нагадування"
                                                                 message:@"Встановіть час"
                                                                   style:LGAlertViewStyleAlert
                                                                    view:datePicker
                                                            buttonTitles:@[@"Done"]
                                                       cancelButtonTitle:@"Cancel"
                                                  destructiveButtonTitle:nil
                                                           actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                               NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
                                                               [outputFormatter setDateFormat:@"HH:mm"];
                                                               NSString *dateString = [outputFormatter stringFromDate:datePicker.date];
                                                               NSString *getSleepTime = [[NSString alloc] initWithFormat:@" %@", outputFormatter];
                                                               NSUserDefaults *timeDefaults = [NSUserDefaults standardUserDefaults];
                                                               UILocalNotification *note = [[UILocalNotification alloc] init];
                                                               note.alertBody = @"Не запізнись на пару";
                                                               note.fireDate = datePicker.date;
                                                               note.timeZone = [NSTimeZone defaultTimeZone];
                                                               //schedule the notification:
                                                               UIApplication *app = [UIApplication sharedApplication];
                                                               UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert) categories:nil];
                                                               [app registerUserNotificationSettings:settings];
                                                               [app scheduleLocalNotification:note];
                                                               
                                                               
                                                               NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                           }
                                                           cancelHandler:^(LGAlertView *alertView) {
                                                               NSLog(@"cancelHandler");
                                                           }
                                                      destructiveHandler:^(LGAlertView *alertView) {
                                                          NSLog(@"destructiveHandler");
                                                      }] showAnimated:YES completionHandler:nil];
                          }
                          cancelHandler:^(LGAlertView *alertView) {
                              NSLog(@"cancelHandler");
                          }
                     destructiveHandler:^(LGAlertView *alertView) {
                         groupPicker =[UIPickerView new];
                         groupPicker.dataSource = self;
                         groupPicker.delegate = self;
                         groupPicker.frame = CGRectMake(0.f, 0.f, self.view.frame.size.width, 110.f);
                         [[[LGAlertView alloc] initWithViewAndTitle:@"Виберіть ваш інститут"
                                                            message:@"та групу зі списку"
                                                              style:LGAlertViewStyleActionSheet
                                                               view:groupPicker
                                                       buttonTitles:@[@"Done"]
                                                  cancelButtonTitle:@"Cancel"
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          //                                                                       [self loadGroups];
                                                          [self readDataMonday];
                                                          [self readDataTuesday];
                                                          [self readDataWednesday];
                                                          [self readDataTherday];
                                                          [self readDataFriday];
                                                          
                                                          NSLog(@"just do it");
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                          NSLog(@"cancelHandler");
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                     NSLog(@"destructiveHandler");
                                                 }] showAnimated:YES completionHandler:nil];
                     }] showAnimated:YES completionHandler:nil];
}
#pragma mark - PickerView
#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    return _arrayGroups.count;
    
}


#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
    defaults = [NSUserDefaults standardUserDefaults];
    NSInteger selectedRow = [pickerView selectedRowInComponent:component];
    NSString *key = [NSString stringWithFormat:@"picker%d", component];
    NSLog(@"key: %@", key);
    [defaults setInteger:selectedRow forKey:key];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
  
   return [[self.arrayGroups objectAtIndex:row] valueForKey:@"group_full_name"];
}
-(void)viewWillAppear: (BOOL) animated {
   defaults = [NSUserDefaults standardUserDefaults];
    [groupPicker selectRow:[defaults integerForKey:@"picker0"]
          inComponent:0 animated:YES];
    [sectionOne reloadData];
    [sectionTwo reloadData];
    [sectionThree reloadData];
    [sectionFour reloadData];
    [sectionFive reloadData];
//    [groupPicker selectRow:[defaults integerForKey:@"picker1"]
//          inComponent:1 animated:YES];
}



#pragma mark - Load Data

#pragma mark DataObjects

-(void)readDataMonday{
    NSString *urlString = @"http://api.rozklad.org.ua/v1/groups/ia-23/lessons";
    NSURL *url = [NSURL URLWithString:urlString];
    urlString = [urlString stringByAppendingString:@"lessons"];
    url = [NSURL URLWithString:urlString];
    //    self.InstituteName = @"11";
    defaults = [NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:@"picker0"];
    NSInteger height = [defaults integerForKey:@"week"];
    NSString *countWeek = [NSString stringWithFormat:@"%li",(long)height];
    self.groupName = userName;
    //        self.groupName = @"ia-23";
    urlString = [NSString stringWithFormat:@"http://api.rozklad.org.ua/v1/groups/%@/", _groupName];
    url = [NSURL URLWithString:urlString];
    NSURL *baseURL = [NSURL URLWithString:urlString];
    NSDictionary *parameters = [[NSDictionary alloc  ] initWithObjectsAndKeys:countWeek, @"week", @"1", @"day", nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:@"lessons/" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        self.dataStoredMonday = responseObject;
        
        [sectionOne reloadData];
        NSLog(@"lessons : %@", _dataStoredMonday);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Виберіть групу в налаштуваннях"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }];
 

}


-(void)readDataTuesday{
    
    NSString *urlString = @"http://api.rozklad.org.ua/v1/groups/ia-23/lessons";
    NSURL *url = [NSURL URLWithString:urlString];
    urlString = [urlString stringByAppendingString:@"lessons"];
    url = [NSURL URLWithString:urlString];
    //    self.InstituteName = @"11";
    defaults = [NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:@"picker0"];
    NSInteger height = [defaults integerForKey:@"week"];
    NSString *countWeek = [NSString stringWithFormat:@"%li",(long)height];
    self.groupName = userName;
    //        self.groupName = @"ia-23";
    urlString = [NSString stringWithFormat:@"http://api.rozklad.org.ua/v1/groups/%@/", _groupName];
    url = [NSURL URLWithString:urlString];
    NSURL *baseURL = [NSURL URLWithString:urlString];
    NSDictionary *parameters = [[NSDictionary alloc  ] initWithObjectsAndKeys: countWeek, @"week", @"2", @"day", nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:@"lessons/" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        self.dataStoredTuesday = responseObject;
        
        [sectionTwo reloadData];

        NSLog(@"lessons : %@", _arrayFromAFNetworking);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Виберіть групу в налаштуваннях"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }];

}

-(void)readDataWednesday{
    
    NSString *urlString = @"http://api.rozklad.org.ua/v1/groups/ia-23/lessons";
    NSURL *url = [NSURL URLWithString:urlString];
    urlString = [urlString stringByAppendingString:@"lessons"];
    url = [NSURL URLWithString:urlString];
    //    self.InstituteName = @"11";
    defaults = [NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:@"picker0"];
    NSInteger height = [defaults integerForKey:@"week"];
    NSString *countWeek = [NSString stringWithFormat:@"%li",(long)height];
    self.groupName = userName;
    //        self.groupName = @"ia-23";
    urlString = [NSString stringWithFormat:@"http://api.rozklad.org.ua/v1/groups/%@/", _groupName];
    url = [NSURL URLWithString:urlString];
    NSURL *baseURL = [NSURL URLWithString:urlString];
    NSDictionary *parameters = [[NSDictionary alloc  ] initWithObjectsAndKeys: countWeek, @"week", @"3", @"day", nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:@"lessons/" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        self.dataStoredWednesday = responseObject;
        
        [sectionThree reloadData];
        NSLog(@"lessons : %@", _arrayFromAFNetworking);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Виберіть групу в налаштуваннях"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }];
}

-(void)readDataTherday{
    NSString *urlString = @"http://api.rozklad.org.ua/v1/groups/ia-23/lessons";
    NSURL *url = [NSURL URLWithString:urlString];
    urlString = [urlString stringByAppendingString:@"lessons"];
    url = [NSURL URLWithString:urlString];
    //    self.InstituteName = @"11";
    defaults = [NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:@"picker0"];
    NSInteger height = [defaults integerForKey:@"week"];
    NSString *countWeek = [NSString stringWithFormat:@"%li",(long)height];
    self.groupName = userName;
    //        self.groupName = @"ia-23";
    urlString = [NSString stringWithFormat:@"http://api.rozklad.org.ua/v1/groups/%@/", _groupName];
    url = [NSURL URLWithString:urlString];
    NSURL *baseURL = [NSURL URLWithString:urlString];
    NSDictionary *parameters = [[NSDictionary alloc  ] initWithObjectsAndKeys:countWeek, @"week", @"4", @"day", nil];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:@"lessons/" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        self.dataStoredThersday = responseObject;
        
        [sectionFour reloadData];
        NSLog(@"lessons : %@", _arrayFromAFNetworking);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Виберіть групу в налаштуваннях"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }];
    
}

-(void)readDataFriday{
 
    NSString *urlString = @"http://api.rozklad.org.ua/v1/groups/ia-23/lessons";
    NSURL *url = [NSURL URLWithString:urlString];
    urlString = [urlString stringByAppendingString:@"lessons"];
    url = [NSURL URLWithString:urlString];
    //    self.InstituteName = @"11";
    defaults = [NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults objectForKey:@"picker0"];
    NSInteger height = [defaults integerForKey:@"week"];
    NSString *countWeek = [NSString stringWithFormat:@"%li",(long)height];
    self.groupName = userName;
    //        self.groupName = @"ia-23";
    urlString = [NSString stringWithFormat:@"http://api.rozklad.org.ua/v1/groups/%@/", _groupName];
    url = [NSURL URLWithString:urlString];
    NSURL *baseURL = [NSURL URLWithString:urlString];
    NSDictionary *queryParameters = [[NSDictionary alloc  ] initWithObjectsAndKeys: @"1", @"week", @"5", @"day", nil];

    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:@"lessons/" parameters:queryParameters  success:^(NSURLSessionDataTask *task, id responseObject) {
        self.dataStoredFriday = responseObject;
        
        [sectionFive reloadData];
        NSLog(@"lessons : %@", _arrayFromAFNetworking);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Виберіть групу в налаштуваннях"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {


    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (tableView == sectionOne) {
        return [self.dataStoredMonday   count];
        NSLog(@"Number of ROW: %@", self.arrayFromAFNetworking);
        
    }
    else if (tableView == sectionTwo){
        return [self.dataStoredTuesday count];
    }
    else if (tableView == sectionThree){
        
        return [self.dataStoredWednesday   count];
    }
    else if (tableView == sectionFour){
        return [self.dataStoredThersday   count];
    }
    else if (tableView == sectionFive){
        return [self.dataStoredFriday   count];
    }
      else{
        NSString *loadError = @"Data reading error";
        return loadError;
    };
//    return 2;
}
//
//
//
- (UITableViewCell *)tableView:(UITableView *)tableView  cellForRowAtIndexPath:(NSIndexPath *)indexPath
{      static NSString *CellIdentifier = @"cell";
    
//    CheduleViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier  forIndexPath:indexPath];
    UITableViewCell *cell =nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    if (tableView == sectionOne) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

        NSDictionary *tempDictionary = [self.dataStoredMonday objectAtIndex:indexPath.row];

        cell.textLabel.font = [UIFont fontWithName:@"Avenir" size:16];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *positionLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 40, 10, 20)];
        positionLabel.textColor = [UIColor blackColor];
        positionLabel.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"lesson_number"]];
        UILabel *NameLabel = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, self.view.bounds.size.width-50, 40)];
        NameLabel.textColor = [UIColor blackColor];
        NameLabel.numberOfLines = 2;
        NameLabel.text = [tempDictionary objectForKey:@"lesson_name"];
        UILabel *TimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(40,70, 120, 20)];
        TimeLabel.textColor = [UIColor blackColor];
        TimeLabel.text = [tempDictionary objectForKey:@"lesson_type"];
        UILabel *roomLabel = [[UILabel alloc]initWithFrame:CGRectMake(240, 70, 80, 20)];
        roomLabel.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"lesson_room"] ];
        UILabel *teacherLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 40, 230, 20)];
        teacherLabel.text = [tempDictionary objectForKey:@"teacher_name"];
        
        [cell addSubview:NameLabel];
        [cell addSubview:TimeLabel];
        [cell addSubview:positionLabel];
        [cell addSubview:roomLabel];
        [cell addSubview:teacherLabel];
        
               NSLog(@"Load cell data: %@", cell);
    }
       else if (tableView == sectionTwo) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
           NSDictionary *tempDictionary = [self.dataStoredTuesday objectAtIndex:indexPath.row];
           cell.textLabel.font = [UIFont fontWithName:@"Avenir" size:16];
           cell.selectionStyle = UITableViewCellSelectionStyleNone;
           UILabel *positionLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 40, 10, 20)];
           positionLabel.textColor = [UIColor blackColor];
           positionLabel.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"lesson_number"]];
           UILabel *NameLabel = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, self.view.bounds.size.width-50, 40)];
           NameLabel.textColor = [UIColor blackColor];
           NameLabel.numberOfLines = 2;
           NameLabel.text = [tempDictionary objectForKey:@"lesson_name"];
           UILabel *TimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(40,70, 120, 20)];
           TimeLabel.textColor = [UIColor blackColor];
           TimeLabel.text = [tempDictionary objectForKey:@"lesson_type"];
           UILabel *roomLabel = [[UILabel alloc]initWithFrame:CGRectMake(240, 70, 80, 20)];
           roomLabel.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"lesson_room"] ];
           UILabel *teacherLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 40, 230, 20)];
           teacherLabel.text = [tempDictionary objectForKey:@"teacher_name"];
           
           [cell addSubview:NameLabel];
           [cell addSubview:TimeLabel];
           [cell addSubview:positionLabel];
           [cell addSubview:roomLabel];
           [cell addSubview:teacherLabel];


            NSLog(@"Load cell data: %@", cell);

        }
       else if (tableView == sectionThree) {
           cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
           
           NSDictionary *tempDictionary = [self.dataStoredWednesday objectAtIndex:indexPath.row];
           cell.textLabel.font = [UIFont fontWithName:@"Avenir" size:16];
           cell.selectionStyle = UITableViewCellSelectionStyleNone;
           UILabel *positionLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 40, 10, 20)];
           positionLabel.textColor = [UIColor blackColor];
           positionLabel.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"lesson_number"]];
           UILabel *NameLabel = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, self.view.bounds.size.width-50, 40)];
           NameLabel.textColor = [UIColor blackColor];
           NameLabel.numberOfLines = 2;
           NameLabel.text = [tempDictionary objectForKey:@"lesson_name"];
           UILabel *TimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(40,70, 120, 20)];
           TimeLabel.textColor = [UIColor blackColor];
           TimeLabel.text = [tempDictionary objectForKey:@"lesson_type"];
           UILabel *roomLabel = [[UILabel alloc]initWithFrame:CGRectMake(240, 70, 80, 20)];
           roomLabel.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"lesson_room"] ];
           UILabel *teacherLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 40, 230, 20)];
           teacherLabel.text = [tempDictionary objectForKey:@"teacher_name"];
           
           [cell addSubview:NameLabel];
           [cell addSubview:TimeLabel];
           [cell addSubview:positionLabel];
           [cell addSubview:roomLabel];
           [cell addSubview:teacherLabel];
           NSLog(@"Load cell data: %@", cell);
       }
       else if (tableView == sectionFour) {
           cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
           
           NSDictionary *tempDictionary = [self.dataStoredThersday objectAtIndex:indexPath.row];
           cell.textLabel.font = [UIFont fontWithName:@"Avenir" size:16];
           cell.selectionStyle = UITableViewCellSelectionStyleNone;
           UILabel *positionLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 40, 10, 20)];
           positionLabel.textColor = [UIColor blackColor];
           positionLabel.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"lesson_number"]];
           UILabel *NameLabel = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, self.view.bounds.size.width-50, 40)];
           NameLabel.textColor = [UIColor blackColor];
           NameLabel.numberOfLines = 2;
           NameLabel.text = [tempDictionary objectForKey:@"lesson_name"];
           UILabel *TimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(40,70, 120, 20)];
           TimeLabel.textColor = [UIColor blackColor];
           TimeLabel.text = [tempDictionary objectForKey:@"lesson_type"];
           UILabel *roomLabel = [[UILabel alloc]initWithFrame:CGRectMake(240, 70, 80, 20)];
           roomLabel.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"lesson_room"] ];
           UILabel *teacherLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 40, 230, 20)];
           teacherLabel.text = [tempDictionary objectForKey:@"teacher_name"];
           
           [cell addSubview:NameLabel];
           [cell addSubview:TimeLabel];
           [cell addSubview:positionLabel];
           [cell addSubview:roomLabel];
           [cell addSubview:teacherLabel];

           NSLog(@"Load cell data: %@", cell);
       }
       else if (tableView == sectionFive) {
           NSDictionary *tempDictionary = [self.dataStoredFriday objectAtIndex:indexPath.row];
           cell.textLabel.font = [UIFont fontWithName:@"Avenir" size:16];
           cell.selectionStyle = UITableViewCellSelectionStyleNone;
           UILabel *positionLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 40, 10, 20)];
           positionLabel.textColor = [UIColor blackColor];
           positionLabel.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"lesson_number"]];
           UILabel *NameLabel = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, self.view.bounds.size.width-50, 40)];
           NameLabel.textColor = [UIColor blackColor];
           NameLabel.numberOfLines = 2;
           NameLabel.text = [tempDictionary objectForKey:@"lesson_name"];
           UILabel *TimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(40,70, 120, 20)];
           TimeLabel.textColor = [UIColor blackColor];
           TimeLabel.text = [tempDictionary objectForKey:@"lesson_type"];
           UILabel *roomLabel = [[UILabel alloc]initWithFrame:CGRectMake(240, 70, 80, 20)];
           roomLabel.text = [NSString stringWithFormat:@"%@", [tempDictionary objectForKey:@"lesson_room"] ];
           UILabel *teacherLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 40, 230, 20)];
           teacherLabel.text = [tempDictionary objectForKey:@"teacher_name"];
           
           [cell addSubview:NameLabel];
           [cell addSubview:TimeLabel];
           [cell addSubview:positionLabel];
           [cell addSubview:roomLabel];
           [cell addSubview:teacherLabel];

           NSLog(@"Load cell data: %@", cell);
       }

    }
    

   return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
//
//
//
//
//#pragma mark - Prepare For Segue
//
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    showDetailsteacherViewController *detailViewController = (UIViewController *)segue.destinationViewController;
    detailViewController.lessonsDetail = [self.arrayFromAFNetworking objectAtIndex:indexPath.row];
}


- (void)viewDidUnload {
    [super viewDidUnload];
}
//
//
//
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//
//
//
- (IBAction)changeWeek:(id)sender {
    if (_segment.selectedSegmentIndex == 1) {
        defaults = [NSUserDefaults standardUserDefaults];
        NSInteger week = 2;
        NSString *countWeek = [NSString stringWithFormat:@"%li",(long)week];
        [defaults setInteger:week forKey:@"week"];
        [defaults synchronize];
        NSLog(@"Week 2");
        [sectionOne reloadData];

    }
    else
    {
        defaults = [NSUserDefaults standardUserDefaults];
        NSInteger week = 1;
        NSString *countWeek = [NSString stringWithFormat:@"%li",(long)week];
        [defaults setInteger:week forKey:@"week"];
        [defaults synchronize];
        NSLog(@"Week 1");
        [sectionOne reloadData];

    }
    
}
@end
        

