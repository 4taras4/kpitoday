//
//  ABLoginVC.m
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABLoginVC.h"
#import "ABAccessToken.h"

@interface ABLoginVC () <UIWebViewDelegate>

@property (copy, nonatomic) ABLoginCompletionBlock completionBlock;
@property (weak, nonatomic) UIWebView *webView;

@end

@implementation ABLoginVC

- (id)initWithCompletionBlock:(ABLoginCompletionBlock)completionBlock {
    
    self = [super init];
    
    if (self) {
        
        self.completionBlock = completionBlock;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect r = self.view.bounds;
    r.origin = CGPointZero;
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:r];
    
    webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    [self.view addSubview:webView];
    
    self.webView = webView;
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                          target:self
                                                                          action:@selector(actionCancel:)];
    
    [self.navigationItem setRightBarButtonItem:item animated:NO];
    
    self.navigationItem.title = @"Login";
    
    NSString *urlString = @"https://oauth.vk.com/authorize?"
                           "client_id=5319641&"
                           "scope=405526&"//+2+4+16+131072+8192+262144+4096 //401430//405526//466966
                           "redirect_uri=https://oauth.vk.com/blank.html&"
                           "display=mobile&"
                           "v=5.37&"
                           "response_type=token"; //&revoke=1
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    webView.delegate = self;
    
    [webView loadRequest:request];
}

- (void)dealloc {
    
    self.webView.delegate = nil;
}

- (void)actionCancel:(UIBarButtonItem *)item {
    
    if (self.completionBlock) {
        self.completionBlock(nil);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if ([[[request URL] description] rangeOfString:@"#access_token="].location != NSNotFound) {
        
        ABAccessToken *token = [[ABAccessToken alloc] init];
       
        NSString *query = [[request URL] description];
        
        NSArray *array = [query componentsSeparatedByString:@"#"];
        
        if ([array count] > 1) {
            query = [array lastObject];
        }
        
        NSArray *pairs = [query componentsSeparatedByString:@"&"];
        
        for (NSString *pair in pairs) {
            NSArray *values = [pair componentsSeparatedByString:@"="];
            if ([values count] == 2) {
                
                NSString *key = [values firstObject];
                if ([key isEqualToString:@"access_token"]) {
                    token.token = [values lastObject];
                    
                } else if ([key isEqualToString:@"expires_in"]) {
                    NSTimeInterval interval = [[values lastObject] doubleValue];
                    
                    token.expirationDate = [NSDate dateWithTimeIntervalSinceNow:interval];
                    
                } else if ([key isEqualToString:@"user_id"]) {
                    token.userID = [values lastObject];
                }
            }
        }

        self.webView.delegate = nil;
        
        if (self.completionBlock) {
            self.completionBlock(token);
            
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
        return NO;
 
    }

   // NSLog(@"%@", [request URL]);
        
    return YES;

}


@end
