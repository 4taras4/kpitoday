//
//  ABLoadServer.h
//  APIDZ-46
//
//  Created by tarik on 12.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@interface ABLoadServer : ABServerObject

@property (strong, nonatomic) NSString *uploadUrl;


@end
