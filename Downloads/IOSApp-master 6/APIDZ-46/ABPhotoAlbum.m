//
//  ABPhotoAlbum.m
//  APIDZ-46
//
//  Created by tarik on 06.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABPhotoAlbum.h"

@implementation ABPhotoAlbum

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {
        
        
        self.sizePhotos = [[responseObject objectForKey:@"size"] integerValue];
        
        self.albumId = [responseObject objectForKey:@"id"];
        
        self.titlePhotosAlbum = [responseObject objectForKey:@"title"];
        
        NSTimeInterval unixtime = [[responseObject objectForKey:@"updated"] doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixtime];
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd.MM.yyyy HH:mm"];
        
        self.dateUpdated = [formatter stringFromDate:date];
        
    }
    return self;
}



@end
