//
//  ABSidebarTableViewController.m
//  APIDZ-46
//
//  Created by tarik on 03.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABSidebarTableViewController.h"
#import <SWRevealViewController.h>
#import "ABWallGroupTVC.h"

#import "Masonry.h"


@interface ABSidebarTableViewController ()

@property (strong, nonatomic) NSArray *menuItems;

@end

@implementation ABSidebarTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.menuItems = [[NSArray alloc] initWithObjects:@"menuGroups",@"test", @"developmentCourse",@"cheduleGroups", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = [self.menuItems objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    /*  NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController *)segue.destinationViewController;
    destViewController.title = [[self.menuItems objectAtIndex:indexPath.row] capitalizedString];  */
    if ([segue.identifier isEqualToString:@"showWallGroup"]) {
        //UINavigationController *navController = segue.destinationViewController;
        //ABWallGroupTVC *wallGroupTVC = [navController childViewControllers].firstObject;
        //NSString *photoFilename = [NSString stringWithFormat:@"%@_photo", [self.menuItems objectAtIndex:indexPath.row]];
        //photoController.photoFilename = photoFilename;
    }
    if ([segue.identifier isEqualToString:@"test"]) {
        //UINavigationController *navController = segue.destinationViewController;
        //ABWallGroupTVC *wallGroupTVC = [navController childViewControllers].firstObject;
        //NSString *photoFilename = [NSString stringWithFormat:@"%@_photo", [self.menuItems objectAtIndex:indexPath.row]];
        //photoController.photoFilename = photoFilename;
    }
    /*
    if ([segue.identifier isEqualToString:@"showMap"]) {
        UINavigationController *navController = segue.destinationViewController;
        MapViewController *photoController = [navController childViewControllers].firstObject;
        NSString *photoFilename = [NSString stringWithFormat:@"%@_map", [self.menuItems objectAtIndex:indexPath.row]];
        //photoController.photoFilename = photoFilename;
    }
    
    if ([segue.identifier isEqualToString:@"showNews"]) {
        UINavigationController *navController = segue.destinationViewController;
        PhotoViewController *photoController = [navController childViewControllers].firstObject;
        NSString *photoFilename = [NSString stringWithFormat:@"%@_news", [self.menuItems objectAtIndex:indexPath.row]];
        //photoController.photoFilename = photoFilename;
    }
     */
}

@end
