//
//  ABPlayer.h
//  APIDZ-46
//
//  Created by tarik on 28.10.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@interface ABVideo : ABServerObject


@property (strong, nonatomic) NSURL *photo320;
@property (strong, nonatomic) NSString *titleVideo;
@property (strong, nonatomic) NSString *dateUpdated;
@property (strong, nonatomic) NSURL *player;


@end
