//
//  ABCommentsPostTVC.m
//  APIDZ-46
//
//  Created by tarik on 19.09.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABCommentsPostTVC.h"
#import "ABCommentsCell1.h"
#import "ABCommentsCell2.h"
#import "ABCommentsCell3.h"
#import "ABCommentsCell4.h"
#import "ABCommentsCell5.h"
#import "ABWall.h"
#import "ABUser.h"
#import "ABGroup.h"
#import "ABCommentsPost.h"
#import "ABServerManager.h"
#import "UIImageView+AFNetworking.h"
#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>
#import "SLKTextViewController.h"
#import "SLKTextInputbar.h"
#import "SLKTextView.h"
#import "ABWallGroupTVC.h"



@interface ABCommentsPostTVC()

@property (strong, nonatomic) NSString *ownerId;
@property (strong, nonatomic) NSMutableArray *commentsArray;
@property (strong, nonatomic) UITextView *textViewComment;
@property (strong, nonatomic) UIBarButtonItem *senderButton;
@property (strong, nonatomic) UIButton *senderButtonCell;
@property (strong, nonatomic) UIToolbar *toolbar;
@property (strong, nonatomic) UIView *viewBar;
@property (nonatomic, readonly) SLKTextInputbar *textInputbar;
@property (nonatomic, strong) SLKTextView *textView;

@property (assign, nonatomic) NSString *commentId;

@property (strong, nonatomic) UIButton *dellCommentButton;


@end

@implementation ABCommentsPostTVC

static NSInteger countComments = 20;


#pragma mark - UITableViewDataSource

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textViewComment.delegate = self;
    
    [self refreshController];
    
    self.ownerId = @"-23762795";
    
    self.commentsArray = [NSMutableArray array];
    
    //[self initToolbarEndTextView];
    
    [self getCommentsFromServer];
    
    
}

- (void)refreshController {
    UIRefreshControl *refreshControlBottom = [[UIRefreshControl alloc] init];
    refreshControlBottom.triggerVerticalOffset = 100.;
    [refreshControlBottom addTarget:self action:@selector(refreshBottom:) forControlEvents:UIControlEventValueChanged];
    //[self.tableView addSubview:refreshControlBottom];
    self.tableView.bottomRefreshControl = refreshControlBottom;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self initToolBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.toolbar removeFromSuperview];

    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillShow:(NSNotification*)notification {

    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    
    self.tableView.contentInset = contentInsets;
    

    self.tableView.scrollIndicatorInsets = contentInsets;
    
     //[self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    CGRect rect = self.fakeFooterView.frame;
    rect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(rect, self.fakeFooterView.frame.origin)) {
        
        CGPoint scrollPoint = CGPointMake(0.0, self.fakeFooterView.frame.origin.y -(keyboardSize.height + self.fakeFooterView.frame.size.height));
        NSLog(@"scrollPoint = %f, %f", scrollPoint.x, scrollPoint.y);
        
        [self.tableView setContentOffset:scrollPoint animated:NO];
        //[self.tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    
    CGRect frame = self.toolbar.frame;
    
    frame.origin.y = self.parentViewController.view.frame.size.height - keyboardSize.height - self.toolbar.frame.size.height;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    /*
    self.tableView.frame= CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height - 190);
    NSIndexPath *indexPath =[NSIndexPath indexPathForRow:nIndex inSection:nSectionIndex];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
     */
    
    //[self.tableView setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
    
    self.toolbar.frame = frame;
    
    //self.tableView.frame = rect;
    /*
    // Cette portion de code permet de remonter le scroll (à cause de la toolbar)
    if (![[AppKit sharedInstance] isIPad]) {
        CGRect tableFrame = self.tableView.frame;
        tableFrame.origin.y = tableFrame.origin.y - 50;
        self.tableView.frame = tableFrame;
    }
     */
    
    [UIView commitAnimations];
    
    // Action pour les keyboards
    //self.toolbarDoneButton.tag = 1;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    CGRect frame = self.toolbar.frame;
    frame.origin.y = self.parentViewController.view.frame.size.height;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    
    self.toolbar.frame = frame;
    /*
    // Cette portion de code permet de rebaisser le scroll (à cause de la toolbar)
    if (![[AppKit sharedInstance] isIPad]) {
        CGRect tableFrame = self.tableView.frame;
        tableFrame.origin.y = tableFrame.origin.y + 50;
        self.tableView.frame = tableFrame;
    }
    */
    [UIView commitAnimations];
}

- (void)initToolBar {
    
    self.toolbar = [[UIToolbar alloc] init];
  
    self.toolbar.barStyle = UIBarStyleDefault;
    
    
    
    //Set the toolbar to fit the width of the app.
    [self.toolbar sizeToFit];
    
    
    
    //Caclulate the height of the toolbar
    
    CGFloat toolbarHeight = [self.toolbar frame].size.height;

    //Get the bounds of the parent view
    
    CGRect viewBounds = self.parentViewController.view.bounds;
    
    //Get the height of the parent view.

    CGFloat rootViewHeight = CGRectGetHeight(viewBounds);
  
    //Get the width of the parent view,
   
    CGFloat rootViewWidth = CGRectGetWidth(viewBounds);
  
    //Create a rectangle for the toolbar
    
    CGRect rectArea = CGRectMake(0, rootViewHeight - toolbarHeight, rootViewWidth, toolbarHeight);
  
    //Reposition and resize the receiver
    
    [self.toolbar setFrame:rectArea];
    
    [self.navigationController.view addSubview:self.toolbar];
    
    self.textViewComment = [[UITextView alloc] init];
    
    self.textViewComment.frame = CGRectMake(20, 10, self.toolbar.frame.size.width - 80, 25);
    self.textViewComment.backgroundColor = [UIColor colorWithRed:235 green:215 blue:210 alpha:1];
    [self.toolbar addSubview:self.textViewComment];
    
    self.senderButton = [[UIBarButtonItem alloc]initWithTitle:@"Send" style:UIBarButtonItemStylePlain target:self action:@selector(senderAction)];
    self.senderButton.tintColor = [UIColor blackColor];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *buttons = [NSArray arrayWithObjects:flex, self.senderButton, nil];
    [self.toolbar setItems: buttons animated:NO];


}


- (void)initToolbarEndTextView {
    
    //Initialize the toolbar
   // self.toolbar = [[UIToolbar alloc] init];
    
    //self.toolbar.frame = CGRectMake(0, 0, self.viewBar.frame.size.width, 44);
    
    
    //self.toolbar.frame = CGRectMake(0, self.tableView.frame.size.height, self.view.frame.size.width, 44);
    
   // [self.viewBar addSubview:self.toolbar];
    /*
    self.textViewComment = [[UITextView alloc] init];
    
    self.textViewComment.frame = CGRectMake(20, 10, self.toolbar.frame.size.width - 80, 25);
    self.textViewComment.backgroundColor = [UIColor colorWithRed:235 green:215 blue:210 alpha:1];
    [self.toolbar addSubview:self.textViewComment];
    
    self.senderButton = [[UIBarButtonItem alloc]initWithTitle:@"Send" style:UIBarButtonItemStylePlain target:self action:@selector(senderAction)];
    self.senderButton.tintColor = [UIColor blackColor];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *buttons = [NSArray arrayWithObjects:flex, self.senderButton, nil];
    [self.toolbar setItems: buttons animated:NO];
     */
}

- (void)refreshBottom:(UIRefreshControl *)refreshControl {
    
    [self updateComments];
    
    [refreshControl endRefreshing];
}

- (void)updateComments {
    
    [[ABServerManager shareManager] getCommentsFromPostId:self.wall.postId ownerId:self.ownerId count:MAX(10, [self.commentsArray count]) offset:0 onSuccess:^(NSArray *array) {
        
        [self.commentsArray removeAllObjects];
        
        [self.commentsArray addObjectsFromArray:array];
        
        NSSortDescriptor *date = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
        
        [self.commentsArray sortUsingDescriptors:[NSArray arrayWithObject:date]];
        
        [self.tableView reloadData];
        
        //[self initToolbarEndTextView];
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

#pragma mark - Actions

- (IBAction)delCommentAction:(UIButton *)sender {
    
    [[ABServerManager shareManager]deleteCommentFromPostId:self.commentId ownerId:self.ownerId onSuccess:^(NSArray *array) {
        
        if (array) {
            [self updateComments];
        }

    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

- (void)tapGestureForButton:(UIButton *)sender {
    
    sender.userInteractionEnabled = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapImageView:)];
    [sender addGestureRecognizer:recognizer];
    
}

- (void)onTapImageView:(UITapGestureRecognizer *)recognizer {
    
    UIImageView *imageView = (UIImageView *)[recognizer view];
    
    UITableViewCell *cell = (UITableViewCell *)[[imageView superview]superview];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    if (indexPath.section == 0) {
        
        NSString *type = @"post";
        
        [[ABServerManager shareManager] addLikeFromObjectType:type ownerId:self.ownerId itemId:self.wall.postId  onSuccess:^(NSArray *array) {
            NSLog(@"like post");
            if (array) {
                
                [self.delegate refreshPost];
                
                [self requestPost];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
        }];
    } else {
        
        NSString *type = @"comment";
        
        ABCommentsPost *commentPost = [self.commentsArray objectAtIndex:indexPath.row];
        
        NSLog(@"commentId - %@", [NSString stringWithFormat:@"%@", commentPost.commentId]);
        
        
        [[ABServerManager shareManager] addLikeFromObjectType:type ownerId:self.ownerId itemId:commentPost.commentId onSuccess:^(NSArray *array) {
            
            if (array) {
                
                [self updateLikeCommentsForPaths:indexPath];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
        }];
    }
}

- (void)updateLikeCommentsForPaths:(NSIndexPath *)indexPath {
    
    [[ABServerManager shareManager] getCommentsFromPostId:self.wall.postId ownerId:self.ownerId count:MAX(10, [self.commentsArray count]) offset:0 onSuccess:^(NSArray *array) {
        
        [self.commentsArray removeAllObjects];
        
        [self.commentsArray addObjectsFromArray:array];
        
        NSSortDescriptor *date = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
        
        [self.commentsArray sortUsingDescriptors:[NSArray arrayWithObject:date]];

        [self.tableView beginUpdates];
        
        NSArray* rowsTobeReloaded = [NSArray arrayWithObjects:indexPath, nil];
        
        [self.tableView reloadRowsAtIndexPaths:rowsTobeReloaded withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tableView endUpdates];
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

- (IBAction)likeAction:(UIButton *)sender {
    
    [self tapGestureForButton:sender];
    
}

- (void)senderAction {
    
    [[ABServerManager shareManager] addCommentFromPostId:self.wall.postId ownerId:self.ownerId text:self.textViewComment.text onSuccess:^(NSArray *array) {

        [self updateComments];
        self.textViewComment.text = nil;

    } onFailure:^(NSError *error, NSInteger statusCode) {
         NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}


- (void)getCommentsFromServer {
    
    [[ABServerManager shareManager] getCommentsFromPostId:self.wall.postId ownerId:self.ownerId count:countComments offset:[self.commentsArray count] onSuccess:^(NSArray *array) {
        
        [self.commentsArray addObjectsFromArray:array];
        
        NSMutableArray *newPaths = [NSMutableArray array];
        
        for (int i = (int)[self.commentsArray count] - (int)[array count]; i < [self.commentsArray count]; i++) {
            
            [newPaths addObject:[NSIndexPath indexPathForItem:i inSection:1]];
        }
        
        [self.tableView beginUpdates];
        
        [self.tableView insertRowsAtIndexPaths:newPaths withRowAnimation:UITableViewRowAnimationFade];
        
        [self.tableView endUpdates];
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return [self.commentsArray count];
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier1 = @"ABCommentsCell1";
    
    static NSString *identifier2 = @"ABCommentsCell2";
    
    static NSString *identifier3 = @"ABCommentsCell3";
    
    static NSString *identifier4 = @"ABCommentsCell4";
    
    static NSString *identifier5 = @"ABCommentsCell5";
    
    if (indexPath.section == 0) {
        if (self.wall.photo) {
            ABCommentsCell1 *cell = [tableView dequeueReusableCellWithIdentifier:identifier1];
            
            if (self.wall.user.firstName || self.wall.user.lastName) {
                cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", self.wall.user.firstName, self.wall.user.lastName];
            } else {
                cell.nameLabel.text = self.wall.group.groupName;
            }
            
            cell.textWallLabel.text = self.wall.text;
            
            cell.dateLabel.text = self.wall.date;
            cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)self.wall.likesCount];
            cell.comments.text = [NSString stringWithFormat:@"%ld", (long)self.wall.commentsCount];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:self.wall.photo];
            
            __weak ABCommentsCell1 *weakCell = cell;
            
            cell.photoView.image = nil;
            
            [cell.photoView setImageWithURLRequest:request
                                  placeholderImage:nil
                                           success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                               weakCell.photoView.image = image;
                                               [weakCell layoutSubviews];
                                           } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                               
                                           }];
            
            NSURLRequest *requestAvatar = nil;
            
            if (self.wall.user.photoMediumURL) {
                requestAvatar = [NSURLRequest requestWithURL:self.wall.user.photoMediumURL];
            } else {
                requestAvatar = [NSURLRequest requestWithURL:self.wall.group.photoMediumURL];
            }
            
            __weak ABCommentsCell1 *weakAvatarCell = cell;
            
            cell.avatar.image = nil;
            
            [cell.avatar setImageWithURLRequest:requestAvatar
                               placeholderImage:nil
                                        success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                            weakAvatarCell.avatar.image = image;
                                            [weakAvatarCell layoutSubviews];
                                            CALayer *imageLayer = weakAvatarCell.avatar.layer;
                                            [imageLayer setCornerRadius:30];
                                            [imageLayer setMasksToBounds:YES];
                                        } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                            
                                        }];
            
            return cell;
            
        } else {
            
            ABCommentsCell2 *cell = [tableView dequeueReusableCellWithIdentifier:identifier2];
            
            
            if (self.wall.user.firstName || self.wall.user.lastName) {
                cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", self.wall.user.firstName, self.wall.user.lastName];
            } else {
                cell.nameLabel.text = self.wall.group.groupName;
            }
            cell.textWallLabel.text = self.wall.text;
            cell.dateLabel.text = self.wall.date;
            cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)self.wall.likesCount];
            cell.comments.text = [NSString stringWithFormat:@"%ld", (long)self.wall.commentsCount];
            
            NSURLRequest *requestAvatar = nil;
            
            if (self.wall.user.photoMediumURL) {
                requestAvatar = [NSURLRequest requestWithURL:self.wall.user.photoMediumURL];
            } else {
                requestAvatar = [NSURLRequest requestWithURL:self.wall.group.photoMediumURL];
            }
            
            __weak ABCommentsCell2 *weakAvatarCell = cell;
            
            cell.avatar.image = nil;
            
            [cell.avatar setImageWithURLRequest:requestAvatar
                               placeholderImage:nil
                                        success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                            weakAvatarCell.avatar.image = image;
                                            [weakAvatarCell layoutSubviews];
                                            CALayer *imageLayer = weakAvatarCell.avatar.layer;
                                            [imageLayer setCornerRadius:30];
                                            [imageLayer setMasksToBounds:YES];
                                        } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                            
                                        }];
            
            return cell;
        }
        
        
    } else {
        
        ABCommentsPost *comments = [self.commentsArray objectAtIndex:indexPath.row];
        
        if ([comments.type isEqualToString:@"link"]) {
            ABCommentsCell4 *cell = [tableView dequeueReusableCellWithIdentifier:identifier4];
            
            
            if (comments.user.firstName || comments.user.lastName) {
                cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", comments.user.firstName, comments.user.lastName];
            } else {
                cell.nameLabel.text = self.wall.group.groupName;
            }
            cell.textCommentsLabel.text = comments.text;
            cell.titleLabel.text = comments.titleLink;
            cell.urlLabel.text = comments.urlLink;
            cell.descriptionLabel.text = comments.descriptionLink;
            cell.dateLabel.text = comments.date;
            cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)comments.likesCount];
            
            NSURLRequest *requestAvatar = nil;
            
            if (comments.user.imageURL) {
                requestAvatar = [NSURLRequest requestWithURL:comments.user.imageURL];
            } else {
                requestAvatar = [NSURLRequest requestWithURL:self.wall.group.photoMediumURL];
            }
            
            __weak ABCommentsCell4 *weakAvatarCell = cell;
            
            cell.avatar.image = nil;
            
            [cell.avatar setImageWithURLRequest:requestAvatar
                               placeholderImage:nil
                                        success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                            weakAvatarCell.avatar.image = image;
                                            [weakAvatarCell layoutSubviews];
                                            CALayer *imageLayer = weakAvatarCell.avatar.layer;
                                            [imageLayer setCornerRadius:30];
                                            [imageLayer setMasksToBounds:YES];
                                        } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                            
                                        }];
            
            NSURLRequest *requestPhotoLink = [NSURLRequest requestWithURL:comments.photoLink];
            
            __weak ABCommentsCell4 *weakPhotoCell = cell;
            
            cell.photoImage.image = nil;
            
            [cell.photoImage setImageWithURLRequest:requestPhotoLink
                                   placeholderImage:nil
                                            success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                                weakPhotoCell.photoImage.image = image;
                                                [weakPhotoCell layoutSubviews];
                                            } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                                
                                            }];
            
            cell.delCommentButton.hidden = YES;
            
            return cell;
            
        } else if ([comments.type isEqualToString:@"photo"]) {
            ABCommentsCell5 *cell = [tableView dequeueReusableCellWithIdentifier:identifier5];
            
            if (comments.user.firstName || comments.user.lastName) {
                cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", comments.user.firstName, comments.user.lastName];
            } else {
                cell.nameLabel.text = self.wall.group.groupName;
            }
            cell.textCommentsLabel.text = comments.text;
            cell.dateLabel.text = comments.date;
            cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)comments.likesCount];
            
            NSURLRequest *requestAvatar = nil;
            
            if (comments.user.imageURL) {
                requestAvatar = [NSURLRequest requestWithURL:comments.user.imageURL];
            } else {
                requestAvatar = [NSURLRequest requestWithURL:self.wall.group.photoMediumURL];
            }
            
            __weak ABCommentsCell5 *weakAvatarCell = cell;
            
            cell.avatar.image = nil;
            
            [cell.avatar setImageWithURLRequest:requestAvatar
                               placeholderImage:nil
                                        success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                            weakAvatarCell.avatar.image = image;
                                            [weakAvatarCell layoutSubviews];
                                            CALayer *imageLayer = weakAvatarCell.avatar.layer;
                                            [imageLayer setCornerRadius:30];
                                            [imageLayer setMasksToBounds:YES];
                                        } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                            
                                        }];
            
            NSURLRequest *requestPhotoLink = [NSURLRequest requestWithURL:comments.photoLink];
            
            __weak ABCommentsCell5 *weakPhotoCell = cell;
            
            cell.photoImage.image = nil;
            
            [cell.photoImage setImageWithURLRequest:requestPhotoLink
                                   placeholderImage:nil
                                            success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                                weakPhotoCell.photoImage.image = image;
                                                [weakPhotoCell layoutSubviews];
                                            } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                                
                                            }];
            
            cell.delCommentButton.hidden = YES;
            
            return cell;
        
        } else {
        
        
        ABCommentsCell3 *cell = [tableView dequeueReusableCellWithIdentifier:identifier3];
        
        
        if (comments.user.firstName || comments.user.lastName) {
            cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", comments.user.firstName, comments.user.lastName];
        } else {
            cell.nameLabel.text = self.wall.group.groupName;
        }
        cell.textCommentsLabel.text = comments.text;
        cell.dateLabel.text = comments.date;
        cell.likeLabel.text = [NSString stringWithFormat:@"%ld", (long)comments.likesCount];
        
        NSURLRequest *requestAvatar = nil;
        
        if (comments.user.imageURL) {
            requestAvatar = [NSURLRequest requestWithURL:comments.user.imageURL];
        } else {
            requestAvatar = [NSURLRequest requestWithURL:self.wall.group.photoMediumURL];
        }
        
        __weak ABCommentsCell3 *weakAvatarCell = cell;
        
        cell.avatar.image = nil;
        
        [cell.avatar setImageWithURLRequest:requestAvatar
                           placeholderImage:nil
                                    success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                                        weakAvatarCell.avatar.image = image;
                                        [weakAvatarCell layoutSubviews];
                                        CALayer *imageLayer = weakAvatarCell.avatar.layer;
                                        [imageLayer setCornerRadius:30];
                                        [imageLayer setMasksToBounds:YES];
                                    } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                                        
                                    }];

            cell.delCommentButton.hidden = YES;
            
        return cell;
        }
    
    }

    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 2) {
        
        return 60.0f;
    }
    else {
    
        return 10.0f;
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if (section == 2) {
        
        return self.fakeFooterView;
    }
    
    else {
        
        return nil;
        
    }
}



- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section > 0) {
        
        ABCommentsPost *commentPost = [self.commentsArray objectAtIndex:indexPath.row];
        
        self.commentId = commentPost.commentId;
        
        ABCommentsCell3 *cellSelected3 = [tableView cellForRowAtIndexPath: indexPath];
        
        cellSelected3.delCommentButton.hidden = NO;
        
        ABCommentsCell4 *cellSelected4 = [tableView cellForRowAtIndexPath: indexPath];
        
        cellSelected4.delCommentButton.hidden = NO;
        
        ABCommentsCell5 *cellSelected5 = [tableView cellForRowAtIndexPath: indexPath];
        
        cellSelected5.delCommentButton.hidden = NO;
    }
    

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section > 0) {
        ABCommentsCell3 *cellSelected = [tableView cellForRowAtIndexPath: indexPath];
        
        cellSelected.delCommentButton.hidden = YES;
        
        ABCommentsCell4 *cellSelected4 = [tableView cellForRowAtIndexPath: indexPath];
        
        cellSelected4.delCommentButton.hidden = YES;
        
        ABCommentsCell5 *cellSelected5 = [tableView cellForRowAtIndexPath: indexPath];
        
        cellSelected5.delCommentButton.hidden = YES;
    }
}

//request of object from a wall
- (void)requestPost {
    NSString *posts = [NSString stringWithFormat:@"-23762795_%@", self.wall.postId];
    
    [[ABServerManager shareManager]getWallById:posts onSuccess:^(id result) {
        self.wall = result;
        
        NSLog(@"TEST!!!");
        
        if (result) {
            NSIndexSet *set = [NSIndexSet indexSetWithIndex:0];
            [self.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
        }
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}


#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    [textView resignFirstResponder];
    
    return YES;
}

- (IBAction)dismissKeyboard:(id)sender {
    
    [self textViewShouldEndEditing:self.textViewComment];
}


@end
