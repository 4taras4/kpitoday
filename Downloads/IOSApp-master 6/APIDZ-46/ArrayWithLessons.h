//
//  ArrayWithLessons.h
//  Today
//
//  Created by tarik on 19.04.16.
//  Copyright © 2016 Alex Bukharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArrayWithLessons : NSObject
@property(weak, nonatomic) NSString *teacher;
@property(weak, nonatomic) NSString *room;
@property(weak, nonatomic) NSString *subject;
@property(assign, nonatomic) NSInteger *week;
@property(assign, nonatomic) NSInteger *day;
@property(assign, nonatomic) NSInteger *position;
@property(weak, nonatomic)NSArray *arrayFromAFNetworking;


@end
