//
//  ABMessageVC.h
//  APIDZ-46
//
//  Created by tarik on 30.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABAddPostDelegate;

@interface ABAddPostVC : UIViewController


@property (weak, nonatomic) id <ABAddPostDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextView *textMessage;
@property (strong, nonatomic) NSString *ownerId;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

- (IBAction)cancelAction:(UIBarButtonItem *)sender;

- (IBAction)saveAction:(UIBarButtonItem *)sender;


@end

@protocol ABAddPostDelegate <NSObject>

- (void)updateRequest;

@end