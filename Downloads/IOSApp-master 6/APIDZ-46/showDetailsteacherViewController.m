//
//  showDetailsteacherViewController.m
//  Today
//
//  Created by tarik on 05.04.16.
//  Copyright © 2016 tarik. All rights reserved.
//

#import "showDetailsteacherViewController.h"
#import "AFNetworking.h"

@interface showDetailsteacherViewController ()
@property (weak, nonatomic) IBOutlet UILabel *endTIme;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *subjectDetails;
@property (weak, nonatomic) IBOutlet UILabel *typeDetails;
@property (weak, nonatomic) IBOutlet UILabel *roomDetails;
@property (weak, nonatomic) IBOutlet UILabel *teacherDetail;

@end

@implementation showDetailsteacherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"Деталі";
    self.subjectDetails.text= [self.lessonsDetail objectForKey:@"lesson_full_name"];
     self.typeDetails.text= [self.lessonsDetail objectForKey:@"lesson_type"];
    self.roomDetails.text = [self.lessonsDetail objectForKey:@"lesson_room"];
    
     self.teacherDetail.text= [self.lessonsDetail objectForKey:@"teacher_name"];
    self.startTime.text= [self.lessonsDetail objectForKey:@"time_start"];
    self.endTIme.text= [self.lessonsDetail objectForKey:@"time_end"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
