//
//  ABPhotoCell.m
//  APIDZ-46
//
//  Created by tarik on 06.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABPhotoCell.h"

@implementation ABPhotoCell

- (id)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder]) {
        // Update checkbox with unchecked image by default
        self.checked = NO;
    }
    return self;
}

// A setter method for checked property
- (void)setChecked:(BOOL)checked {
    // Save property value
    _checked = checked;
    
    // Update checkbox image
    if(checked) {
        self.checkBoxImageView.image = [UIImage imageNamed:@"Checked"];
    } else {
        self.checkBoxImageView.image = [UIImage imageNamed:@"Unchecked"];
    }
}

@end
