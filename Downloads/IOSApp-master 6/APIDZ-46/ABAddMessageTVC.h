//
//  ABAddMessageTVC.h
//  APIDZ-46
//
//  Created by Аtarik on 06.09.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSQMessagesViewController/JSQMessages.h>
#import <JSQMessagesViewController/JSQMessagesBubbleImageFactory.h>


@class ABItemMessage;
@class ABUser;

@interface ABAddMessageTVC : JSQMessagesViewController <UIActionSheetDelegate>

@property (strong, nonatomic) NSURL *avatarIncoming;
@property (strong, nonatomic) NSURL *avatarOutgoing;



- (IBAction)cancelAction:(UIBarButtonItem *)sender;



@end
