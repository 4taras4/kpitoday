//
//  ABCameraVC.m
//  APIDZ-46
//
//  Created by Александр on 14.11.15.
//  Copyright © 2015 Alex Bukharov. All rights reserved.
//

#import "ABCameraVC.h"
#import "ABServerManager.h"
#import "ABPhotoAlbum.h"
#import "ABLoadServer.h"
#import "ABSavePhotos.h"


@interface ABCameraVC () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (strong, nonatomic) UIImage *chosenImage;

@end

@implementation ABCameraVC

static NSString *ownerId = @"23762795";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.savePhotoButton.enabled = NO;
    // Do any additional setup after loading the view.
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        [self showAlertController];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    self.chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = self.chosenImage;
    
    if (self.chosenImage) {
        self.savePhotoButton.enabled = YES;
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)showAlertController {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Error"
                                  message:@"Device has no camera"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Actions

- (IBAction)backAction:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)CameraAction:(UIBarButtonItem *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)folderAction:(UIBarButtonItem *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)savePhotoAction:(UIBarButtonItem *)sender {
    
    [[ABServerManager shareManager]getPhotosUploadServerFromGroupId:ownerId albumId:self.photoAlbum.albumId onSuccess:^(id result) {
        ABLoadServer *loadServer = result;
        
        [self postPhotoUploadURL:loadServer.uploadUrl];
        
        NSLog(@"URL = %@", loadServer.uploadUrl);
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}



- (void)postPhotoUploadURL:(NSString *)uploadURL {
    
    NSData *imageData = UIImageJPEGRepresentation(self.chosenImage, 1.0f);
    
    [[ABServerManager shareManager]postPhotoUploadUrl:uploadURL
                                            imageData:imageData
                                            onSuccess:^(id result) {
                                                
                                                ABSavePhotos *savePhotos = result;
                                                
                                                [[ABServerManager shareManager]savePhotosForAlbumId:savePhotos.albumId
                                                                                            groupId:savePhotos.groupId
                                                                                             server:savePhotos.server
                                                                                         photosList:savePhotos.photosList
                                                                                               hash:savePhotos.hashPhoto
                                                                                          onSuccess:^(id result) {
                                                                                              
                                                                                              self.savePhotoButton.enabled = NO;
                                                                                              
                                                                                              [self.delegate updateRequest];
                                                                                              
                                                                                              //ABPhotoCell* cell = (ABPhotoCell*) [self.collectionView cellForItemAtIndexPath:self.checkedIndexPath];
                                                                                              
                                                                                              //cell.checked = NO;
                                                                                              
                                                                                              [self showAlertControllerSavePhoto];
                                                                                              
                                                                                          } onFailure:^(NSError *error, NSInteger statusCode) {
                                                                                              NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
                                                                                              
                                                                                          }];
                                                
                                            }
                                            onFailure:^(NSError *error, NSInteger statusCode) {
                                                NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
                                            }];
}

#pragma mark - UIAlertController

- (void)showAlertControllerSavePhoto {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@"Фото сохранено"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    /*
     UIAlertAction* noButton = [UIAlertAction
     actionWithTitle:@"No, thanks"
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     */
    
    [alert addAction:yesButton];
    //[alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end
