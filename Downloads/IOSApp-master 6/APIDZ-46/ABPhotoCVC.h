//
//  ABPhotoCVC.h
//  APIDZ-46
//
//  Created by tarik on 06.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABPhotoAlbum;


@interface ABPhotoCVC : UICollectionViewController

@property (strong, nonatomic) ABPhotoAlbum *photoAlbum;

- (IBAction)backAction:(UIBarButtonItem *)sender;

@end
