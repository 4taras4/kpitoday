//
//  ABCommentsPostTVC.h
//  APIDZ-46
//
//  Created by tarik on 19.09.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABCommentsPostDelegate;


@class ABWall;

@interface ABCommentsPostTVC : UITableViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>


@property (weak, nonatomic) id <ABCommentsPostDelegate> delegate;

@property (strong, nonatomic) ABWall *wall;


@property (weak, nonatomic) IBOutlet UIView *fakeFooterView;


- (IBAction)delCommentAction:(UIButton *)sender;


- (IBAction)likeAction:(UIButton *)sender;



@end

@protocol ABCommentsPostDelegate <NSObject>

- (void)refreshPost;

@end
