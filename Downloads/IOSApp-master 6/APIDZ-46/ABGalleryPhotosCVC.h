//
//  ABGalleryPhotosCVC.h
//  APIDZ-46
//
//  Created by Александр on 08.11.15.
//  Copyright © 2015 Alex Bukharov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABGalleryPhotosDelegate;

@class ABPhotoAlbum;

@interface ABGalleryPhotosCVC : UICollectionViewController

@property (weak, nonatomic) id <ABGalleryPhotosDelegate> delegate;

@property (strong, nonatomic) NSString *path;
@property (strong, nonatomic) NSIndexPath* checkedIndexPath;
@property (strong, nonatomic) NSString* checkedObject;
@property (strong, nonatomic) ABPhotoAlbum *photoAlbum;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addPhoto;


- (id)initWithFolderPath:(NSString *)path nibNameOrNil:(NSString *)nibNameOrNil nibBundleOrNil:(NSBundle *)nibBundleOrNil;


- (IBAction)backAction:(UIBarButtonItem *)sender;

- (IBAction)actionButtonAddPhoto:(UIBarButtonItem *)sender;

@end


@protocol ABGalleryPhotosDelegate <NSObject>

- (void)updateRequest;

@end