//
//  ABPlayerView.m
//  APIDZ-46
//
//  Created by tarik on 27.10.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "ABPlayerView.h"

@implementation ABPlayerView

+ (Class)layerClass
{
    return [AVPlayerLayer class];
}

- (AVPlayer *)player
{
    return [(AVPlayerLayer *)[self layer] player];
}

- (void)setPlayer:(AVPlayer *)player
{
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}

@end
