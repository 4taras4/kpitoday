//
//  ABWall.m
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABWall.h"

@implementation ABWall

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {
        
        NSTimeInterval unixtime = [[responseObject objectForKey:@"date"] doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixtime];
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd.MM.yyyy HH:mm"];
        
        self.date = [formatter stringFromDate:date];
        
        NSString* string = [responseObject objectForKey:@"text"];
        
        self.text = [string stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        
        NSDictionary *likes = [responseObject objectForKey:@"likes"];
        
        self.likesCount = [[likes objectForKey:@"count"] integerValue];
        
        NSDictionary *comments = [responseObject objectForKey:@"comments"];
        
        self.commentsCount = [[comments objectForKey:@"count"] integerValue];
        
        NSDictionary *attachment = [responseObject objectForKey:@"attachment"];
        
        self.type = [attachment objectForKey:@"type"];
        
        NSDictionary *photo = [attachment objectForKey:@"photo"];
        
        NSString *src = [photo objectForKey:@"src_big"];
        
        NSDictionary *video = [attachment objectForKey:@"video"];
        
        NSString *image = [video objectForKey:@"image"];
        
        if (src) {
            self.photo = [NSURL URLWithString:src];
        }
        
        if (image) {
            self.photo = [NSURL URLWithString:image];
        }
         
        
        self.vid = [[video objectForKey:@"vid"] stringValue];;
        
        self.fromId = [[responseObject objectForKey:@"from_id"] stringValue];
        
        self.postId = [[responseObject objectForKey:@"id"] stringValue];
        
       // NSLog(@"id - %@", self.postId);
        
    }
    return self;
}




@end

//ld: warning: directory not found for option '-F/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator9.0.sdk/Developer/Library/Frameworks'

