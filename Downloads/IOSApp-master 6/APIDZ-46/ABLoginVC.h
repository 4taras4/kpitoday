//
//  ABLoginVC.h
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABAccessToken;

typedef void(^ABLoginCompletionBlock)(ABAccessToken *token);

@interface ABLoginVC : UIViewController

- (id)initWithCompletionBlock:(ABLoginCompletionBlock)completionBlock;

@end
