//
//  ABGalleryPhotosCVC.m
//  APIDZ-46
//
//  Created by Александр on 08.11.15.
//  Copyright © 2015 Alex Bukharov. All rights reserved.
//

#import "ABGalleryPhotosCVC.h"
#import "ABPhotoCell.h"
#import "ABServerManager.h"
#import "ABPhotoAlbum.h"
#import "ABLoadServer.h"
#import "ABSavePhotos.h"

@interface ABGalleryPhotosCVC ()

@property (strong, nonatomic) NSArray *contents;
@property (strong, nonatomic) UIBarButtonItem *itemSelectPhotos;

@end

@implementation ABGalleryPhotosCVC

static NSString *ownerId = @"-23762795";

- (id)initWithFolderPath:(NSString *)path nibNameOrNil:(NSString *)nibNameOrNil nibBundleOrNil:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.path = path;
    }
    return self;          
}

- (void) setPath:(NSString *)path {
    
    _path = path;
    
    NSError *error = nil;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    self.contents =  [fileManager contentsOfDirectoryAtURL:[NSURL fileURLWithPath:self.path]
                                includingPropertiesForKeys:[NSArray arrayWithObject:NSURLNameKey]
                                                   options:NSDirectoryEnumerationSkipsHiddenFiles
                                                     error:&error];
    
    self.contents = [self makePathfromUrlArray:self.contents];
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    
    
    [self.collectionView reloadData];
    
    self.navigationItem.title = [self.path lastPathComponent];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super initWithCoder:aDecoder]) {

        self.checkedIndexPath = [NSIndexPath indexPathWithIndex:0];
    }
    return self;
}

static NSString * const reuseIdentifier = @"CellGallery";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.path) {
        self.path = @"../../Documents/";
    }
    
    self.addPhoto.enabled = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)makePathfromUrlArray:(NSArray *)array {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSURL * item in array) {
        NSString *string = item.path;
        NSString *stringName = [string lastPathComponent];
        [tempArray addObject:stringName];
    }
    return tempArray;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return [self.contents count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ABPhotoCell *cell = (ABPhotoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    NSString *fileName = [self.contents objectAtIndex:indexPath.row];
    
    NSString *thePath = [NSString stringWithFormat:@"%@/%@", self.path, fileName];
    
    //NSString *thePath = [[NSBundle mainBundle] pathForResource:pathForResource ofType:nil inDirectory:self.path];
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:thePath];
    
    cell.imageViewCell.image = image;
    
    cell.checked = [self.checkedIndexPath isEqual:indexPath];
    
    // Set random color on cells (for fun!)
   // cell.backgroundColor = [UIColor randomFlatLightColor];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // Deselect cell first
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    if (self.checkedIndexPath) {
        ABPhotoCell* cell = (ABPhotoCell*) [collectionView cellForItemAtIndexPath:self.checkedIndexPath];
        
        cell.checked = NO;
    }
    if ([self.checkedIndexPath isEqual:indexPath]) {
        self.checkedIndexPath = nil;
        self.checkedObject = nil;
        
        self.addPhoto.enabled = NO;
    } else {
        ABPhotoCell* cell = (ABPhotoCell*) [collectionView cellForItemAtIndexPath:indexPath];
        
        self.checkedIndexPath = indexPath;
        cell.checked = YES;
        self.checkedObject = [self.contents objectAtIndex:indexPath.row];
        
        self.addPhoto.enabled = YES;
        
        NSLog(@"checkedObject - %@", self.checkedObject);
        
        
    }
    
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/
/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

#pragma mark - Actions

- (IBAction)actionButtonAddPhoto:(UIBarButtonItem *)sender {
    
    self.addPhoto.enabled = NO;
    
    [[ABServerManager shareManager]getPhotosUploadServerFromGroupId:ownerId albumId:self.photoAlbum.albumId onSuccess:^(id result) {
        ABLoadServer *loadServer = result;
        
        [self postPhotoUploadURL:loadServer.uploadUrl];
        
        NSLog(@"URL = %@", loadServer.uploadUrl);
    
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

- (void)postPhotoUploadURL:(NSString *)uploadURL {
    
    NSString *thePath = [NSString stringWithFormat:@"%@/%@", self.path, self.checkedObject];
    
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:thePath];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    
    [[ABServerManager shareManager]postPhotoUploadUrl:uploadURL
                                            imageData:imageData
                                            onSuccess:^(id result) {
                                                
                                                ABSavePhotos *savePhotos = result;

                                                [[ABServerManager shareManager]savePhotosForAlbumId:savePhotos.albumId
                                                                                            groupId:savePhotos.groupId
                                                                                             server:savePhotos.server
                                                                                         photosList:savePhotos.photosList
                                                                                               hash:savePhotos.hashPhoto
                                                                                          onSuccess:^(id result) {
                                                                                              
                                                                                              [self.delegate updateRequest];
                                                                                              
                                                                                              ABPhotoCell* cell = (ABPhotoCell*) [self.collectionView cellForItemAtIndexPath:self.checkedIndexPath];
                                                                                              
                                                                                              cell.checked = NO;
                                                                                              
                                                                                              [self showAlertController];
                                                                                              
                                                                                          } onFailure:^(NSError *error, NSInteger statusCode) {
                                                                                              NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
                                                                                              
                                                                                          }];
                                                
                                            }
                                            onFailure:^(NSError *error, NSInteger statusCode) {
                                                NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
                                            }];
}

#pragma mark - UIAlertController

- (void)showAlertController {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@"Фото сохранено"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    /*
     UIAlertAction* noButton = [UIAlertAction
     actionWithTitle:@"No, thanks"
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     */
    
    [alert addAction:yesButton];
    //[alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)backAction:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}




@end
