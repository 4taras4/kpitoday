//
//  TabWeekViewController.h
//  Today
//
//  Created by tarik on 15.03.16.
//  Copyright © 2016 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "ABSidebarTableViewController.h"
#import <SWRevealViewController.h>
#import "AFNetworking.h"
#import "CellsDetails.h"
//#import "NSDictionary+QueryStringBuilder.h"


@interface TabWeekViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UIActionSheetDelegate >{
    
    NSMutableArray *dataStored;
    UITableView *sectionOne;
    UITableView *sectionTwo;
    UITableView *sectionThree;
    UITableView *sectionFour;
    UITableView *sectionFive;
    UITableView *sectionSix;
   
}
- (IBAction)changeWeek:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingBtn;
- (IBAction)setting:(id)sender;

-(void) retrieveData;
-(void) makeRestaurantRequests;
@end
