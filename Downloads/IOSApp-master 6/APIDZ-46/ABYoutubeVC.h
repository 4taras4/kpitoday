//
//  ABYoutubeVC.h
//  APIDZ-46
//
//  Created by tarik on 30.10.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABYoutubeVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *viewVideo;

@property (strong, nonatomic) NSString *identifier;

@end
