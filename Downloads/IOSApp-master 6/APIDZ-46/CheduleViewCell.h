//
//  CheduleViewCell.h
//  Today
//
//  Created by tarik on 14.04.16.
//  Copyright © 2016 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheduleViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *positionView;
@property (weak, nonatomic) IBOutlet UILabel * roomView;
@property (weak, nonatomic) IBOutlet UILabel *typeView;
@property (weak, nonatomic) IBOutlet UILabel *teacherView;
@property (weak, nonatomic) IBOutlet UILabel *subjectView;








@end
