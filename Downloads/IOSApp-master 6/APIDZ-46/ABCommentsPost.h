//
//  CommentsWall.h
//  APIDZ-46
//
//  Created by tarik on 24.09.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@class ABUser;

@interface ABCommentsPost : ABServerObject

@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic) NSInteger likesCount;
@property (assign, nonatomic) NSInteger commentsCount;
@property (strong, nonatomic) NSString *commentId;
@property (strong, nonatomic) NSString *fromId;
@property (strong, nonatomic) ABUser *user;

@property (strong, nonatomic) NSURL *avatar;

@property (strong, nonatomic) NSString *descriptionLink;
@property (strong, nonatomic) NSURL *photoLink;
@property (strong, nonatomic) NSURL *photoPhoto;
@property (strong, nonatomic) NSString *textLink;
@property (strong, nonatomic) NSString *titleLink;
@property (strong, nonatomic) NSString *urlLink;
@property (strong, nonatomic) NSString *type;


@end
