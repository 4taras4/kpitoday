//
//  ABSavePhotos.h
//  APIDZ-46
//
//  Created by tarik on 13.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@interface ABSavePhotos : ABServerObject

@property (strong, nonatomic) NSString *albumId;
@property (strong, nonatomic) NSString *groupId;
@property (strong, nonatomic) NSString *hashPhoto;
@property (strong, nonatomic) NSString *photosList;
@property (strong, nonatomic) NSString *server;


@end
