//
//  ABWallCell2.h
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABWallCell2 : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *textWallLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *comments;

@end
