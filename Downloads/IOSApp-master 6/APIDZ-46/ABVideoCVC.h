//
//  ABVideoCVC.h
//  APIDZ-46
//
//  Created by tarik on 05.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABVideoAlbum;


@interface ABVideoCVC : UICollectionViewController

@property (strong, nonatomic) ABVideoAlbum *videoAlbum;

- (IBAction)backAction:(UIBarButtonItem *)sender;

@end
