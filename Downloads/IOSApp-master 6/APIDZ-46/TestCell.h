//
//  TestCell.h
//  Today
//
//  Created by tarik on 28.03.16.
//  Copyright © 2016 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestCell : NSObject

@property (nonatomic, strong) NSString *CheduleSubject;
@property (nonatomic, strong) NSString *CheduleTeacher;
@property (nonatomic, strong) NSString *CheduleType;
@property (nonatomic, strong) NSString *CheduleRoom;
@property (nonatomic, strong) NSArray *lessons;
@end
