//
//  ABPhoto.h
//  APIDZ-46
//
//  Created by tarik on 06.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@interface ABPhoto : ABServerObject

@property (strong, nonatomic) NSURL *photo130;
@property (strong, nonatomic) NSURL *photo604;
@property (assign, nonatomic) NSInteger countLikes;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *text;


@end


