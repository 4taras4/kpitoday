//
//  CheduleModel.h
//  Today
//
//  Created by tarik on 22.04.16.
//  Copyright © 2016 Alex Bukharov. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface CheduleModel : JSONModel

@property(weak, nonatomic) NSString *teacher;
@property(weak, nonatomic) NSString *room;
@property(weak, nonatomic) NSString *subject;
@property(assign, nonatomic) NSInteger *week;
@property(assign, nonatomic) NSInteger *day;
@property(assign, nonatomic) NSInteger *position;
@property(weak, nonatomic)NSArray *arrayFromAFNetworking;

@end
