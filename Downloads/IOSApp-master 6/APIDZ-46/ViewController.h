//
//  ViewController.h
//  Today
//
//  Created by tarik on 10.03.16.
//  Copyright © 2016 tarik. All rights reserved.
//
#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import "ABSidebarTableViewController.h"
#import <SWRevealViewController.h>
#import <Masonry.h>
#import <EVTTabPageScrollView.h>
//#import <NSFetchedResultsControllerDelegate>

@interface ViewController : UIViewController //<NSFetchedResultsControllerDelegate> {
   // NSFetchedResultsController *fetchedResultsController;
    //NSManagedObjectContext *managedObjectContext;
    
    /* (...Existing Application Code...) */


//@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
//@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@end
