//
//  ABServerManager.m
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABServerManager.h"
#import <AFNetworking.h>
#import "ABWall.h"
#import "ABUser.h"
#import "ABGroup.h"
#import "ABCommentsPost.h"
#import "ABLoginVC.h"
#import "ABItemMessage.h"
#import <JSQMessage.h>
#import "ABAccessToken.h"
#import "ABVideo.h"
#import "ABVideoAlbum.h"
#import "ABPhotoAlbum.h"
#import "ABPhoto.h"
#import "ABLoadServer.h"
#import "ABSavePhotos.h"

@interface ABServerManager ()

@property (strong, nonatomic) AFHTTPRequestOperationManager *requestOperationManager;
@property (strong, nonatomic) ABAccessToken *accessToken;


@end

@implementation ABServerManager

+ (ABServerManager *)shareManager {
    
    static ABServerManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ABServerManager alloc] init];
    });
    return manager;
}

- (id) init {
    self = [super init];
    if (self) {
        
        NSURL *url = [NSURL URLWithString:@"https://api.vk.com/method/"];
        
        self.requestOperationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    }
    return  self;
}

- (void)authorizeUser:(void(^)(ABUser *user))completion {
    
    ABLoginVC *vc = [[ABLoginVC alloc] initWithCompletionBlock:^(ABAccessToken *token) {
        
        self.accessToken = token;
        
        if (token) {
            
            [self getUser:self.accessToken.userID
                onSuccess:^(ABUser *user) {
                    if (completion) {
                        completion(user);
                    }
                }
                onFailure:^(NSError *error, NSInteger statusCode) {
                    if (completion) {
                        completion(nil);
                    }
                }];
        } else if (completion) {
            completion(nil);
        }
    }];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    UIViewController *mainVC = [[[[UIApplication sharedApplication] windows] firstObject] rootViewController];
    
    [mainVC presentViewController:nav animated:YES completion:nil];
}


- (void) getUser:(NSString *)userID
       onSuccess:(void(^)(ABUser *user))success
       onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     userID,      @"user_ids", //26955116
     @"photo_50", @"fields",
     @"nom",      @"name_case", nil];
    
    [self.requestOperationManager
     GET:@"users.get"
     parameters:params
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         //NSLog(@"JSON: %@", responseObject);
         
         NSArray *dictsArray = [responseObject objectForKey:@"response"];
         
         if ([dictsArray count] > 0) {
             ABUser *user = [[ABUser alloc] initWithServerResponse:[dictsArray firstObject]];
             if (success) {
                 success(user);
             }
         } else {
             if (failure) {
                 failure(nil, operation.response.statusCode);
             }
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         
         if (failure) {
             failure(error, operation.response.statusCode);
         }
     }];
}

- (void)getWallWithUserId:(NSString *)ownerId
                   offset:(NSInteger)offset
                    count:(NSInteger)count
                onSuccess:(void (^)(id result))success
                onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    if (![ownerId hasPrefix:@"-"]) {
        ownerId = [@"-" stringByAppendingString:ownerId];
    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            ownerId,     @"owner_id",
                            @(count),    @"count",
                            @(offset),   @"offset",
                            @"all",      @"filter",
                            @"5.37",     @"version",
                            @1,          @"extended",
                            self.accessToken.token, @"access_token", nil];
    
    [self.requestOperationManager GET:@"wall.get"
                          parameters:params
                             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                 
                                 //NSLog(@"JSON: %@", responseObject);
                                 
                                 NSDictionary *responseDict = [responseObject objectForKey:@"response"];
                                 
                                 NSArray *wallArray = [responseDict objectForKey:@"wall"];
                       
                                 NSMutableArray *arr = [NSMutableArray arrayWithArray:wallArray];
                                 [arr removeObjectAtIndex:0];
                                 
                                 NSArray *profilesArray = [responseDict objectForKey:@"profiles"];
                                 
                                 NSMutableArray *arrayKey = [NSMutableArray array];
                                 
                                 for (int i = 0; i < [profilesArray count]; i++) {
                                     NSDictionary *dictProf = [profilesArray objectAtIndex:i];
                                     
                                     NSString *key = [NSString stringWithFormat:@"%@",[dictProf valueForKey:@"uid"]];
                                     
                                     [arrayKey addObject:key];
                                 }
                                 
                                 NSDictionary *dict = [[NSDictionary alloc] initWithObjects:profilesArray forKeys:arrayKey];
                                 
                                 NSArray *groupsArray = [responseDict objectForKey:@"groups"];
                                 
                                 NSDictionary *groupsDictionary = [groupsArray firstObject];
                    
                                 
                                 NSMutableArray *objectArray = [NSMutableArray array];
                                 
                                 for (int i = 0; i < [arr count]; i++) {
                                     
                                     ABWall *wall = [[ABWall alloc] initWithServerResponse:[arr objectAtIndex:i]];
                                    
                                     NSString *uid = [NSString stringWithFormat:@"%@",[[arr objectAtIndex:i] valueForKey:@"from_id"]];
                                     NSDictionary *dictionaryUser = nil;
                                     dictionaryUser = [dict valueForKey:uid];
                                     
                                     wall.user = [[ABUser alloc] initWithServerResponse:dictionaryUser];
                                     wall.group = [[ABGroup alloc] initWithServerResponse:groupsDictionary];
                                     
                                     [objectArray addObject:wall];
                                 }

                                 if (success) {
                                     success(objectArray);
                                 }
                                 
                             } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
                                 
                                 if (failure) {
                                     failure(error, operation.response.statusCode);
                                 }
                             }];
}

- (void)postWallOwnerId:(NSString *)ownerId
                message:(NSString *)message
              onSuccess:(void(^)(id result))success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     self.accessToken.token, @"access_token",
     ownerId,      @"owner_id",
     message,      @"message",
     @"5.37",      @"v", nil];
    
    [self.requestOperationManager POST:@"wall.post"
                            parameters:params
                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                   //NSLog(@"JSON: %@", responseObject);
                                   
                                   if (success) {
                                       success(responseObject);
                                   }
                                   
                               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                   NSLog(@"Error: %@", error);
                                   
                                   if (failure) {
                                       failure(error, operation.response.statusCode);
                                   }
                               }];
}

- (void)sendMessageForUserId:(NSString *)userId
                     message:(NSString *)message
                   onSuccess:(void(^)(id result))success
                   onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     self.accessToken.token, @"access_token",
     userId,              @"user_id",
     message,                @"message",
     @"5.37",                @"v", nil];
    
    [self.requestOperationManager GET:@"messages.send"
                            parameters:params
                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                   //NSLog(@"JSON SEND: %@", responseObject);
                                   
                                   if (success) {
                                       success(responseObject);
                                   }
                                   
                               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                   NSLog(@"Error: %@", error);
                                   
                                   if (failure) {
                                       failure(error, operation.response.statusCode);
                                   }
                               }];
}

- (void)getHistoryMessagesFromUserId:(NSString *)userId
                          senderName:(NSString*)senderName
                               count:(NSInteger)count
                              offset:(NSInteger)offset
                           onSuccess:(void(^)(NSArray *array))success
                           onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.accessToken.token, @"access_token",
                            userId,                 @"user_id",
                            @(count),               @"count",
                            @(offset),              @"offset",
                            @"5.37",                @"v",nil];
    
    [self.requestOperationManager GET:@"messages.getHistory"
                           parameters:params
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  //NSLog(@"JSON SEND: %@", responseObject);
                                  
                                  NSDictionary *responseDict = [responseObject objectForKey:@"response"];
                                  
                                  NSArray *itemsArray = [responseDict objectForKey:@"items"];
                                  
                                  NSMutableArray *objectArray = [NSMutableArray array];
                                  
                                  for (int i = 0; i < [itemsArray count]; i++) {
                                      
                                      NSString *senderId = [[[itemsArray objectAtIndex:i] objectForKey:@"from_id"] stringValue];
                                      
                                      NSTimeInterval unixtime = [[[itemsArray objectAtIndex:i] objectForKey:@"date"] doubleValue];
                                      
                                      NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixtime];
     
                                      
                                      NSString *text = [[itemsArray objectAtIndex:i] objectForKey:@"body"];
                                      
                                      JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                                                               senderDisplayName:senderName
                                                                                            date:date
                                                                                            text:text];
                                      
                                      // NSLog(@"wall %@", wall);
                                      
                                      [objectArray addObject:message];
                                  }
                                  
                                  if (success) {
                                      success(objectArray);
                                  }
                                  
                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                  NSLog(@"Error: %@", error);
                                  
                                  if (failure) {
                                      failure(error, operation.response.statusCode);
                                  }
                              }];
}

- (void)getCommentsFromPostId:(NSString *)postId
                      ownerId:(NSString *)ownerId
                        count:(NSInteger)count
                       offset:(NSInteger)offset
                    onSuccess:(void(^)(NSArray *array))success
                    onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.accessToken.token, @"access_token",
                            postId,                 @"post_id",
                            ownerId,                @"owner_id",
                            @(count),               @"count",
                            @(offset),              @"offset",
                            @1,                     @"extended",
                            @1,                     @"need_likes",
                            @"5.37",                @"v",nil];
    
    [self.requestOperationManager GET:@"wall.getComments"
                           parameters:params
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  //NSLog(@"JSON SEND: %@", responseObject);
                                  
                                  NSDictionary *responseDict = [responseObject objectForKey:@"response"];
                                  
                                  NSArray *profilesArray = [responseDict objectForKey:@"profiles"];
                                  
                                  NSMutableArray *arrayKey = [NSMutableArray array];
                                  
                                  for (int i = 0; i < [profilesArray count]; i++) {
                                      NSDictionary *dictProf = [profilesArray objectAtIndex:i];
                                      
                                      NSString *key = [NSString stringWithFormat:@"%@",[dictProf valueForKey:@"id"]];
                                      
                                      [arrayKey addObject:key];
                                  }
                                  
                                  NSDictionary *dict = [[NSDictionary alloc] initWithObjects:profilesArray forKeys:arrayKey];
                                  
                                  NSArray *itemsArray = [responseDict objectForKey:@"items"];
                                  
                                  NSMutableArray *objectArray = [NSMutableArray array];
                                  
                                  for (int i = 0; i < [itemsArray count]; i++) {
                                      
                                      ABCommentsPost *comments = [[ABCommentsPost alloc] initWithServerResponse:[itemsArray objectAtIndex:i]];
                                      
                                      NSString *uid = [NSString stringWithFormat:@"%@",[[itemsArray objectAtIndex:i] valueForKey:@"from_id"]];
                                      NSDictionary *dictionaryUser = nil;
                                      dictionaryUser = [dict valueForKey:uid];
                                      
                                      comments.user = [[ABUser alloc] initWithServerResponse:dictionaryUser];
                                      
                                      [objectArray addObject:comments];
                                  }
                                  
                                  if (success) {
                                      success(objectArray);
                                  }
                                  
                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                  NSLog(@"Error: %@", error);
                                  
                                  if (failure) {
                                      failure(error, operation.response.statusCode);
                                  }
                              }];
}

//wall.addComment

- (void)addCommentFromPostId:(NSString *)postId
                     ownerId:(NSString *)ownerId
                        text:(NSString *)text
                   onSuccess:(void(^)(NSArray *array))success
                   onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.accessToken.token, @"access_token",
                            postId,                 @"post_id",
                            ownerId,                @"owner_id",
                            text,                   @"text",
                            @"5.37",                @"v",nil];
    
    [self.requestOperationManager POST:@"wall.addComment"
                            parameters:params
                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                    //NSLog(@"JSON: %@", responseObject);
                                   
                                   if (success) {
                                       success(responseObject);
                                   }
                                   
                               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                   NSLog(@"Error: %@", error);
                                   
                                   if (failure) {
                                       failure(error, operation.response.statusCode);
                                   }
                               }];
    
}

//wall.deleteComment

- (void)deleteCommentFromPostId:(NSString *)commentId
                        ownerId:(NSString *)ownerId
                      onSuccess:(void(^)(NSArray *array))success
                      onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.accessToken.token, @"access_token",
                            commentId,              @"comment_id",
                            ownerId,                @"owner_id",
                            @"5.37",                @"v",nil];
    
    [self.requestOperationManager POST:@"wall.deleteComment"
                            parameters:params
                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                   //NSLog(@"JSON: %@", responseObject);
                                   
                                   if (success) {
                                       success(responseObject);
                                   }
                                   
                               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                   NSLog(@"Error: %@", error);
                                   
                                   if (failure) {
                                       failure(error, operation.response.statusCode);
                                   }
                               }];
    
}

//likes.add
- (void)addLikeFromObjectType:(NSString *)type
                         ownerId:(NSString *)ownerId
                          itemId:(NSString *)itemId
                       onSuccess:(void(^)(NSArray *array))success
                       onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            type,                   @"type",
                            ownerId,                @"owner_id",
                            itemId,                 @"item_id",
                            @"5.40",                @"v",
                            self.accessToken.token, @"access_token", nil];
    
    [self.requestOperationManager POST:@"likes.add"
                            parameters:params
                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                   //NSLog(@"JSON: %@", responseObject);
                                   
                                   if (success) {
                                       success(responseObject);
                                   }
                                   
                               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                   NSLog(@"Error: %@", error);
                                   
                                   if (failure) {
                                       failure(error, operation.response.statusCode);
                                   }
                               }];
    
}

//wall.getById

- (void)getWallById:(NSString *)posts
          onSuccess:(void (^)(id result))success
          onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            posts,     @"posts",
                            @"5.37",     @"version",
                            @1,          @"extended",
                            self.accessToken.token, @"access_token", nil];
    
    [self.requestOperationManager GET:@"wall.getById"
                           parameters:params
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  
                                  //NSLog(@"JSON: %@", responseObject);
                                  
                                  NSDictionary *responseDict = [responseObject objectForKey:@"response"];
                                  
                                  NSArray *wallArray = [responseDict objectForKey:@"wall"];
                                  
                                  NSDictionary *wallDict = [wallArray objectAtIndex:0];
                                  
                                  NSArray *profilesArray = [responseDict objectForKey:@"profiles"];
                                  
                                  NSDictionary *profilesDict = [profilesArray objectAtIndex:0];
                                  
                                  NSLog(@"JSON1: %@", profilesDict);
                                  
                                  ABWall *wall = [[ABWall alloc] initWithServerResponse:wallDict];
                                  
                                  wall.user = [[ABUser alloc] initWithServerResponse:profilesDict];
                                  
                                  if (success) {
                                      success(wall);
                                  }
     
                                  
                              } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
                                  
                                  if (failure) {
                                      failure(error, operation.response.statusCode);
                                  }
                              }];
}

//video.get

- (void)getVideoFromOwnerId:(NSString *)ownerId
                     videos:(NSString *)videos
                     albumId:(NSString *)albumId
                       count:(NSInteger)count
          onSuccess:(void (^)(NSArray *result))success
          onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            ownerId,     @"owner_id",
                            videos,      @"videos",
                            albumId,     @"album_id",
                            @(count),    @"count",
                            @0,          @"offset",
                            @"5.37",     @"v",
                            self.accessToken.token, @"access_token", nil];
    
    [self.requestOperationManager GET:@"video.get"
                           parameters:params
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  
                                 //NSLog(@"JSON: %@", responseObject);
                                  
                                  NSDictionary *responseDict = [responseObject objectForKey:@"response"];
                                  
                                  NSArray *itemsArray = [responseDict objectForKey:@"items"];
                                  
                                  NSMutableArray *objectArray = [NSMutableArray array];
                                  
                                  for (int i = 0; i < [itemsArray count]; i++) {
                                      NSDictionary *itemsDict = [itemsArray objectAtIndex:i];
                                      
                                      ABVideo *video = [[ABVideo alloc] initWithServerResponse:itemsDict];
                                      
                                      [objectArray addObject:video];
                                  }
                                  
                                  if (success) {
                                      success(objectArray);
                                  }
                                  
                                  
                              } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
                                  
                                  if (failure) {
                                      failure(error, operation.response.statusCode);
                                  }
                              }];
}

//video.getAlbums

- (void)getVideoAlbumsOwnerId:(NSString *)ownerId
                       offset:(NSInteger)offset
                       count:(NSInteger)count
                  onSuccess:(void (^)(NSArray *result))success
                  onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            ownerId,     @"owner_id",
                            @(offset),   @"offset",
                            @(count),    @"count",
                            @1,          @"extended",
                            @"5.37",     @"v",
                            self.accessToken.token, @"access_token", nil];
    
    [self.requestOperationManager GET:@"video.getAlbums"
                           parameters:params
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  
                                  //NSLog(@"JSON: %@", responseObject);
                                  
                                  NSDictionary *responseDict = [responseObject objectForKey:@"response"];
                                  
                                  NSArray *itemsArray = [responseDict objectForKey:@"items"];
                                  
                                  NSMutableArray *objectArray = [NSMutableArray array];
                                  
                                  for (int i = 0; i < [itemsArray count]; i++) {
                                      NSDictionary *itemsDict = [itemsArray objectAtIndex:i];
                                      
                                      ABVideoAlbum *videoAlbum = [[ABVideoAlbum alloc] initWithServerResponse:itemsDict];
                                      
                                      [objectArray addObject:videoAlbum];
                                  }

                                  if (success) {
                                      success(objectArray);
                                  }
                                  
                              } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
                                  
                                  if (failure) {
                                      failure(error, operation.response.statusCode);
                                  }
                              }];
}

//photos.getAlbums

- (void)getPhotosAlbumsOwnerId:(NSString *)ownerId
                       offset:(NSInteger)offset
                        count:(NSInteger)count
                    onSuccess:(void (^)(NSArray *result))success
                    onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            ownerId,     @"owner_id",
                            @(offset),   @"offset",
                            @(count),    @"count",
                            @"5.37",     @"v",
                            self.accessToken.token, @"access_token", nil];
    
    [self.requestOperationManager GET:@"photos.getAlbums"
                           parameters:params
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  
                                  //NSLog(@"JSON: %@", responseObject);
                                  
                                  NSDictionary *responseDict = [responseObject objectForKey:@"response"];
                                  
                                  NSArray *itemsArray = [responseDict objectForKey:@"items"];
                                  
                                  NSMutableArray *objectArray = [NSMutableArray array];
                                  
                                  for (int i = 0; i < [itemsArray count]; i++) {
                                      NSDictionary *itemsDict = [itemsArray objectAtIndex:i];
                                      
                                      ABPhotoAlbum *photosAlbum = [[ABPhotoAlbum alloc] initWithServerResponse:itemsDict];
                                      
                                      [objectArray addObject:photosAlbum];
                                  }
                                  
                                  if (success) {
                                      success(objectArray);
                                  }
                                  
                              } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
                                  
                                  if (failure) {
                                      failure(error, operation.response.statusCode);
                                  }
                              }];
}

//photos.get

- (void)getPhotosOwnerId:(NSString *)ownerId
                        albumId:(NSString *)albumId
                          count:(NSInteger)count
                         offset:(NSInteger)offset
                      onSuccess:(void (^)(NSArray *result))success
                      onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            ownerId,     @"owner_id",
                            albumId,     @"album_id",
                            @(offset),   @"offset",
                            @(count),    @"count",
                            @1,          @"extended",
                            @"5.37",     @"v",
                            self.accessToken.token, @"access_token", nil];
    
    [self.requestOperationManager GET:@"photos.get"
                           parameters:params
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  
                                  //NSLog(@"JSON: %@", responseObject);
                                  
                                  NSDictionary *responseDict = [responseObject objectForKey:@"response"];
                                  
                                  NSArray *itemsArray = [responseDict objectForKey:@"items"];
                                  
                                  NSMutableArray *objectArray = [NSMutableArray array];
                                  
                                  for (int i = 0; i < [itemsArray count]; i++) {
                                      NSDictionary *itemsDict = [itemsArray objectAtIndex:i];
                                      
                                      ABPhoto *photo = [[ABPhoto alloc] initWithServerResponse:itemsDict];
                                      
                                      [objectArray addObject:photo];
                                  }
                                  
                                  if (success) {
                                      success(objectArray);
                                  }
                                  
                              } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
                                  
                                  if (failure) {
                                      failure(error, operation.response.statusCode);
                                  }
                              }];
}

//photos.getUploadServer

- (void)getPhotosUploadServerFromGroupId:(NSString *)groupId
                                 albumId:(NSString *)albumId
                               onSuccess:(void (^)(id result))success
                               onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            groupId,     @"group_id",
                            albumId,     @"album_id",
                            @"5.40",     @"v",
                            self.accessToken.token, @"access_token", nil];
    
    [self.requestOperationManager GET:@"photos.getUploadServer"
                           parameters:params
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  
                                 //NSLog(@"JSON2: %@", responseObject);
                                  
                                  NSDictionary *responseDict = [responseObject objectForKey:@"response"];
                                  
                                  ABLoadServer *loadServer = [[ABLoadServer alloc] initWithServerResponse:responseDict];
                                  
                                  if (success) {
                                      success(loadServer);
                                  }
                    
                              } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
                                  
                                  if (failure) {
                                      failure(error, operation.response.statusCode);
                                  }
                              }];
}

- (void)postPhotoUploadUrl:(NSString *)uploadUrl
                 imageData:(NSData *)imageData
                 onSuccess:(void (^)(id result))success
                 onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:uploadUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData name:@"file1" fileName:@"file1.png" mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
       // NSLog(@"JSON2222: %@", responseObject);
        
        ABSavePhotos *savePhotos = [[ABSavePhotos alloc] initWithServerResponse:responseObject];

                                   if (success) {
                                   success(savePhotos);
                                   }
        
                              } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
                                  
                                  if (failure) {
                                      failure(error, operation.response.statusCode);
                                  }
                              }];
}

//photos.save

- (void)savePhotosForAlbumId:(NSString *)albumId
                     groupId:(NSString *)groupId
                      server:(NSString *)server
                  photosList:(NSString *)photosList
                        hash:(NSString *)hash
                   onSuccess:(void (^)(id result))success
                   onFailure:(void (^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            groupId,     @"group_id",
                            albumId,     @"album_id",
                            server,      @"server",
                            photosList,  @"photos_list",
                            hash,        @"hash",
                            @"5.40",     @"v",
                            self.accessToken.token, @"access_token", nil];
    
    [self.requestOperationManager GET:@"photos.save"
                           parameters:params
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  
                                   //NSLog(@"JSON: %@", responseObject);
                                  
                                  if (success) {
                                      success(responseObject);
                                  }
                                  
                                  
                              } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
                                  
                                  if (failure) {
                                      failure(error, operation.response.statusCode);
                                  }
                              }];
}
@end
