//
//  ViewController.h
//  APIDZ-46
//
//  Created by tarik on 27.08.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABWallGroupDelegate;

@interface ABWallGroupTVC : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id <ABWallGroupDelegate> delegate;

@property (strong, nonatomic) NSString *ownerId;

@property (strong, nonatomic) NSURL *avatarUser;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;



@end

@protocol ABWallGroupDelegate <NSObject>

- (void)reloadRows;

@end