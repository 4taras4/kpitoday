//
//  ABGroup.h
//  APIDZ-46
//
//  Created bytarik on 17.09.15.
//  Copyright (c) 2015 tarik. All rights reserved.
//

#import "ABServerObject.h"

@interface ABGroup : ABServerObject

@property (strong, nonatomic) NSString *groupName;
@property (strong, nonatomic) NSURL *photoMediumURL;

- (id)initWithServerResponse:(NSDictionary *)responseObject;

@end
