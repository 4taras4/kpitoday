//
//  AuthorizationVC.m
//  APIDZ-46
//
//  Created by tarik on 04.11.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABAuthorizationVC.h"
#import "ABServerManager.h"
#import "ABUser.h"
#import <SWRevealViewController.h>
#import "ABWallGroupTVC.h"
#import "SWRevealViewController.h"

@interface ABAuthorizationVC ()

@property (assign, nonatomic) BOOL firstTimeApper;
@property (strong, nonatomic) NSURL *avatarUser;

@end

@implementation ABAuthorizationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.firstTimeApper = YES;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if (revealViewController) {
        [self.sidebarButton setTarget:self.revealViewController];
        [self.sidebarButton setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    //self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if (self.firstTimeApper) {
        self.firstTimeApper = NO;
        
        [[ABServerManager shareManager] authorizeUser:^(ABUser *user) {
            
            self.avatarUser = user.imageURL;
            
            [ABServerManager shareManager].avatarUser = user.imageURL;
            
            SWRevealViewController *revealVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
            
            [self presentViewController:revealVC animated:YES completion:nil];
            
            //NSLog(@"AUTHORIZED");
            //NSLog(@"%@ %@", user.lastName, user.firstName);
        }];
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"start"]) {
        
        
    }
}



@end
