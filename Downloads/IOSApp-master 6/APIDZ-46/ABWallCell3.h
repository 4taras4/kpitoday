//
//  ABWallCell3.h
//  APIDZ-46
//
//  Created by tarik on 30.10.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABWallCell3 : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *textWallLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *comments;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;

@end
