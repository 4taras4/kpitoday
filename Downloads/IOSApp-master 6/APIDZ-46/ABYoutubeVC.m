//
//  ABYoutubeVC.m
//  APIDZ-46
//
//  Created by tarik on 30.10.15.
//  Copyright © 2015 tarik. All rights reserved.
//

#import "ABYoutubeVC.h"
#import <XCDYouTubeVideoPlayerViewController.h>

@interface ABYoutubeVC ()

@end

@implementation ABYoutubeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:self.identifier];
    
    [videoPlayerViewController presentInView:self.viewVideo];
    
    [videoPlayerViewController.moviePlayer play];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
