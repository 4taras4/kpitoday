<?
include('kcaptcha/kcaptcha.php');
session_start();
require_once("config.php");
require_once("kcaptcha/util/script.php");

if ($_POST['act']== "y")
{
    if(isset($_SESSION['captcha_keystring']) && $_SESSION['captcha_keystring'] ==  $_POST['keystring'])
    {
        
        if (isset($_POST['name1']) && $_POST['name1'] == "")
        {
         $statusError = "$errors_name";
        }
        elseif (isset($_POST['email1']) && $_POST['email1'] == "")
        {
         $statusError = "$errors_mailfrom";
        }
        elseif(isset($_POST['email1']) && !preg_match("/^([a-z,._,0-9])+@([a-z,._,0-9])+(.([a-z])+)+$/", $_POST['email1']))
        {
         $statusError = "$errors_incorrect";

         unset($_POST['email1']);
        }
       
        elseif (isset($_POST['messages']) && $_POST['messages'] == "")
        {
         $statusError = "$errors_message";
        }

elseif (!empty($_POST))
{   
 $headers  = "MIME-Version: 1.0\r\n";
 $headers .= "Content-Type: $content  charset=$charset\r\n";
 $headers .= "From: \"".$_POST['name1']."\" <".$_POST['email1'].">\r\n";
 $headers .= "X-Mailer: My Send E-mail\r\n";

 mail("$mailto","$subject","$message","$headers");

 unset($name, $posText, $mailto, $subject, $message);

 $statusSuccess = "$send";
}

       }else{
             $statusError = "$captcha_error";
             unset($_SESSION['captcha_keystring']);
        }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Favicon
    ======================================================================== -->
    <link rel="icon" href="images/favicon.ico">

    <!-- Meta
    ======================================================================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Title
    ======================================================================== -->
    <title>S. Smith Landscapes &amp; Driveways - Contact</title>

    <!-- CSS Styles
    ======================================================================== -->
    <link rel="stylesheet" href="css/style.css" media="screen" />
    <link rel="stylesheet" href="css/boxed.css" media="screen" id="layout" />
    <link rel="stylesheet" href="css/colors/color-blue.css" media="screen" id="colors" />
    <link rel="stylesheet" href="css/flexslider.css" media="screen" />
    <link rel="stylesheet" href="css/fancybox.css" media="screen" />
    <link rel="stylesheet" href="css/revslider.css" media="screen" />
    <link rel="stylesheet" href="css/font-awesome.css" media="screen" />
    <link rel="stylesheet" href="css/zocial.css" media="screen" />
    <!--<link rel="stylesheet" href="css/responsive.css" media="screen" />-->

    <!-- JavaScript
    ======================================================================== -->
   <!-- <script src="js/jquery-1.8.2.min.js"></script>-->
    <script src="js/jquery.themepunch.plugins.min.js"></script>
    <script src="js/jquery.themepunch.revolution.min.js"></script>
    <script src="js/jquery.ui.widget.min.js"></script>                          
    <script src="js/jquery.ui.accordion.min.js"></script>
    <script src="js/jquery.ui.tabs.min.js"></script>
    <script src="js/jquery.easing-1.3.min.js"></script>
    <script src="js/jquery.fitvid.js"></script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.imagesloaded.min.js"></script>
    <script src="js/jquery.infinitescroll.min.js"></script>
    <script src="js/jquery.jcarousel.min.js"></script>
    <script src="js/jquery.jtweetsanywhere-1.3.1.min.js"></script>
    <script src="js/jquery.touchSwipe.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/jquery.zflickrfeed.min.js"></script>
    <script src="js/respond.min.js"></script>
    <script src="js/selectnav.min.js"></script>
    <script src="js/custom.js"></script>

    <!-- Styles Selector
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="css/styleselector.css">
    <script src="js/styleselector.js"></script>


    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-32209063-4']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
    

</head>
<body style="background-size:cover">

<!-- Body Wrapper -->
<div id="body-wrapper">


    <!-- Header
            ======================================================================== -->
            <div id="header">

                <!-- Logo -->
                <div class="container">
                    <div class="two-third">
                        <a href="index.html" id="logo"><img src="images/logo.png"/></a>
                    </div>
                    <div class="one-third topright">
                        <ul class="contact">
                            <li><span>Phone</span>024 7699 5965</li>
                            <li><span>Mobile</span> 07551 213325</li>
                        </ul>
                        <p>Call now for a free quote or appointment<br> or email contact@ssmithlandscapes.co.uk</p>
                    </div>
                </div>

                <!-- Navigation -->
                <nav>
                    <div class="container">
                        <ul id="navigation">
                            <li>
                                <a href="index.html">Home</a>
                            </li>

                            <li><a href="about.html">About Us</a></li>
                            <li><a href="services.html">Services</a></li>
                            <li><a href="gallery-and-portfolio.html">Gallery &amp; Portfolio</a></li>
                            <li class="current"><a href="contact.php">Contact</a></li>

                        </ul>
                    </div>
                </nav>
                <!-- Navigation / End -->

            </div>
            <!-- Header / End -->
            <div class="clearfix"></div>


    <!-- Content
    ======================================================================== -->
    <div id="content" class="container">
    
    <hr class="sep60">
    	<h1>Contact Us</h1>
    	<hr class="sep30">

        <!-- Main Content -->
        <div id="main">
               
            <!-- Contact Form -->
            <div class="contact-form">
                
                <form action="contact.php" method="post"  id="contact-form" >
                    <input type="hidden" name="act" value="y" />
<fieldset>
                    <label>Name</label>
                    <input type="text" name="name1" required/>
                    <label>Email</label>
                    <input type="text" name="email1" required/>
                    <label>Message</label>
                    <textarea cols="88" rows="6" name="messages" required></textarea>
                    <div id="q"><label for="posCaptcha"><b>Enter the code</b>:</label>
                    <img src="kcaptcha/index.php?<?php echo session_name()?>=<?php echo session_id()?>"><?php echo $r1; ?><br>
                    <input class="inputIE" type="text" size="47" name="keystring" id="keystring" required/></div>
                    <input type="submit" name="submit" onclick="alert('Message sented!');" value="Submit Form" class="yellow-darkgray">
                 </fieldset>
                 </form>
                  
            </div>
    
        </div>
        
        <!-- Sidebar 
        <div id="sidebar">

            
            <h3 class="headline">Contact info</h3>
            <p class="address">123 Address Street, Town, Coventry, CV</p>
            <p class="phone">Phone: 024 7699 5965 <br />Mobile: 07551 213325</p>
            <p class="email">contact@ssmithlandscapes.co.uk</p>

            Social Links
            <hr class="sep30">
            <h3 class="headline">Social Links</h3>
            <ul class="content-social-links">
                <li><a href="#" class="zocial twitter"></a></li>
                <li><a href="#" class="zocial youtube"></a></li>
                <li><a href="#" class="zocial linkedin"></a></li>
                <li><a href="#" class="zocial pinterest"></a></li>
                <li><a href="#" class="zocial blogger"></a></li>
            </ul>

        </div>
        -->

    </div>
    <!-- Content / End -->


    <!-- Footer
    ======================================================================== -->
    <div id="footer">

        <div class="container">
        	
        	<div id="areas">
			<h3>Specialists in Driveways & Patios in Coventry/Warwickshire and the surrounding areas</h3>
			<p>Coventry - Warwickshire - Ryton-on-Dunsmore - Wolston - Kenilworth - Keresley - Leamington Spa - Bedworth - Long Lawford - Long Itchington - Warwick - Meriden - Dunchurch - Bishop's Tachbrook - Rugby - Hampton in Arden - Harbury - Nuneaton - Wellesbourne</p>
        	</div>

        </div>

        <!-- Info -->
        <div class="info">
            <div class="container">

                <!-- Copyright -->
                <ul class="copyright">
                    <li>© 2015 Copyright All Rights Reserved</li>
                </ul>

                <!-- Social Links
                <ul class="social-links">
                    <li><a href="#" class="zocial twitter"></a></li>
                    <li><a href="#" class="zocial facebook"></a></li>
                    <li><a href="#" class="zocial linkedin"></a></li>
                    <li><a href="#" class="zocial pinterest"></a></li>
                    <li><a href="#" class="zocial vimeo"></a></li>
                </ul>
                -->
                
                <ul class="web">
                    <li><a href="#">Website by JoshCrad</a></li>
                </ul>
                
               

            </div>
        </div>

    </div>
    <!-- Footer / End -->

</div>
<!-- Body Wrapper / End -->

<!-- Back to Top -->
<a href="#" id="back-to-top"><i class="icon-chevron-up"></i></a>


</body></html>




